package com.wisata.app.fragment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.wisata.app.R
import com.wisata.app.activity.ActivityMyWisata
import com.wisata.app.activity.ActivitySignUp
import com.wisata.app.connection.RestAdapter
import com.wisata.app.connection.callbacks.CallbackLogin
import com.wisata.app.connection.callbacks.CallbackProfil
import com.wisata.app.connection.callbacks.DefaultCallback
import com.wisata.app.data.Constant
import com.wisata.app.data.SharedPref
import kotlinx.android.synthetic.main.frame_profil.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentProfil : Fragment() {

    private var rootView: View? = null
    private var sharedPref: SharedPref? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPref = SharedPref(activity!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.frame_profil, null)
            initComponent()
        }
        return rootView
    }

    private fun initComponent() {
        rootView?.btnSignUp?.setOnClickListener {
            ActivitySignUp.navigate(activity!!)
        }
        rootView?.btnLogin?.setOnClickListener { submitLogin() }
        rootView?.btnLogOut?.setOnClickListener { actionLogOut() }
        rootView?.btnWisataSaya?.setOnClickListener { ActivityMyWisata.navigate(activity!!) }
        requestProfil()
    }

    private fun displayLoginPage() {
        rootView?.lytLogin?.visibility = View.VISIBLE
        rootView?.lytProfil?.visibility = View.GONE
    }

    private fun displayProfilPage() {
        rootView?.lytLogin?.visibility = View.GONE
        rootView?.lytProfil?.visibility = View.VISIBLE
    }

    private fun requestProfil() {
        val rest = RestAdapter.createAPI().getProfil()
        rest.enqueue(object : Callback<CallbackProfil> {
            override fun onFailure(call: Call<CallbackProfil>?, t: Throwable?) {
                // can't connect to server or STATUS is not 200
                displayLoginPage()
            }

            override fun onResponse(call: Call<CallbackProfil>?, response: Response<CallbackProfil>?) {
                val resp = response?.body()
                resp?.let {
                    if (it.status!!) {
                        displayProfilPage()
                        sharedPref?.user = it.data
                        rootView?.txtNama?.text = it.data?.nama
                        rootView?.txtEmail?.text = it.data?.email
                        rootView?.txtPhone?.text = it.data?.phone
                        rootView?.txtUsername?.text = it.data?.username
                    } else {
                        displayLoginPage()
                    }
                    return@let
                }
                if (resp == null) {
                    displayLoginPage()
                }
            }
        })
    }

    private fun submitLogin() {
        val username = rootView?.username?.text.toString()
        val password = rootView?.password?.text.toString()
        if (TextUtils.isEmpty(username)) {
            Toast.makeText(activity!!, "Username harus diisi", Toast.LENGTH_SHORT).show()
            rootView?.username?.requestFocus()
        } else if (TextUtils.isEmpty(password)) {
            Toast.makeText(activity!!, "Password harus diisi", Toast.LENGTH_SHORT).show()
            rootView?.password?.requestFocus()
        } else {
            val rest = RestAdapter.createAPI().submitLogin(username = username, password = password)
            rest.enqueue(object : Callback<CallbackLogin> {
                override fun onFailure(call: Call<CallbackLogin>?, t: Throwable?) {
                    // can't connect to server or STATUS is not 200
                    Toast.makeText(activity!!, "Gagal melakukan login", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<CallbackLogin>?, response: Response<CallbackLogin>?) {
                    val resp = response?.body()
                    resp?.let {
                        if (it.status!!) {
                            displayProfilPage()
                            sharedPref?.token = it.apikey
                            sharedPref?.isLogIn = true
                            requestProfil()
                        } else {
                            displayLoginPage()
                            Toast.makeText(activity!!, it.msg, Toast.LENGTH_SHORT).show()
                        }
                        return@let
                    }
                    if (resp == null) {
                        displayLoginPage()
                        Toast.makeText(activity!!, "Gagal melakukan login", Toast.LENGTH_SHORT).show()
                    }
                }
            })
        }
    }

    private fun actionLogOut() {
        val rest = RestAdapter.createAPI().doLogout()
        rest.enqueue(object : Callback<DefaultCallback> {
            override fun onFailure(call: Call<DefaultCallback>?, t: Throwable?) {
                // can't connect to server or STATUS is not 200
                Toast.makeText(activity!!, "Gagal logout", Toast.LENGTH_SHORT).show()
                displayProfilPage()
            }

            override fun onResponse(call: Call<DefaultCallback>?, response: Response<DefaultCallback>?) {
                val resp = response?.body()
                resp?.let {
                    if (it.status!!) {
                        displayLoginPage()
                        sharedPref?.token = Constant.GUEST_API_KEY
                        sharedPref?.user = null
                        Snackbar.make(activity!!.findViewById<View>(android.R.id.content), "Anda berhasil logout", Snackbar.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(activity!!, "Gagal logout", Toast.LENGTH_SHORT).show()
                        displayProfilPage()
                    }
                    return@let
                }
                if (resp == null) {
                    Toast.makeText(activity!!, "Gagal logout", Toast.LENGTH_SHORT).show()
                    displayProfilPage()
                }
            }
        })
    }
}