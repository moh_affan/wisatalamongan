package com.wisata.app.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.CheckBox
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.clustering.ClusterManager
import com.wisata.app.R
import com.wisata.app.activity.ActivityDetailWisata
import com.wisata.app.activity.ActivitySearchResult
import com.wisata.app.adapter.AdapterCheckFasilitas
import com.wisata.app.adapter.AdapterWisataPopuler
import com.wisata.app.connection.RestAdapter
import com.wisata.app.connection.callbacks.CallbackFasilitas
import com.wisata.app.connection.callbacks.CallbackWisata
import com.wisata.app.model.Fasilitas
import com.wisata.app.model.WisataComplete
import com.wisata.app.utils.Tools
import com.wisata.app.widget.ClusteredMarker
import kotlinx.android.synthetic.main.frame_pencarian.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentPencarian : Fragment(), AdapterWisataPopuler.OnItemClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, AdapterCheckFasilitas.OnCheckboxClickListener {
    private var rootView: View? = null
    private var adapter: AdapterWisataPopuler? = null
    private var items: ArrayList<WisataComplete> = arrayListOf()
    private var googleMap: GoogleMap? = null
    private var mLocationRequest: LocationRequest? = null
    private var mLastLocation: Location? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mCurrLocationMarker: Marker? = null
    private var clusterManager: ClusterManager<ClusteredMarker>? = null
    private val qryOptions = hashMapOf(R.id.radioDekat to "j_terdekat",
            R.id.radioJarakSedang to "j_sedang",
            R.id.radioJauh to "j_terjauh",
            R.id.radioMurah to "h_termurah",
            R.id.radioHargaSedang to "h_sedang",
            R.id.radioMahal to "h_termahal")
    private lateinit var adapterCheckFasilitas: AdapterCheckFasilitas
    private val checkedFasilitas = mutableSetOf<Int>()
    private var arrfas: ArrayList<Fasilitas>? = null

    @SuppressLint("InflateParams")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.frame_pencarian, null)
            initComponent()
            initMap(savedInstanceState)
            requestData()
        }
        return rootView
    }

    private fun initComponent() {
        rootView?.edtSearch?.setOnFocusChangeListener { _, hasFocus -> if (hasFocus) rootView?.expandableLayout?.expand(true) }
        rootView?.edtSearch?.setOnClickListener { rootView?.expandableLayout?.expand(true) }
        rootView?.btnExpandLess?.setOnClickListener { rootView?.expandableLayout?.collapse(true) }
        rootView?.edtSearch?.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                actionSearch(rootView?.edtSearch?.text.toString())
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        rootView?.btnActionSearch?.setOnClickListener { actionSearch(rootView?.edtSearch?.text.toString()) }
        rootView?.recyclerView?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        adapter = AdapterWisataPopuler(context!!, items)
        toggleLocationButton(ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        rootView?.btnActivateLocation?.setOnClickListener { checkLocationPermission();checkGpsStatus() }
        rootView?.radioDekat?.setOnClickListener { checkLocationPermission();checkGpsStatus() }
        rootView?.radioJauh?.setOnClickListener { checkLocationPermission();checkGpsStatus() }
        rootView?.btnResetForm?.setOnClickListener { resetRadio();rootView?.edtSearch?.text?.clear() }
        rootView?.btnSubmitSearch?.setOnClickListener { actionSearch(rootView?.edtSearch?.text.toString()) }
        adapterCheckFasilitas = AdapterCheckFasilitas(context!!, arrayListOf())
        rootView?.recyclerCheckFasilitas?.adapter = adapterCheckFasilitas
        adapterCheckFasilitas.setOnCheckboxClickListener(this)
        getFasilitas()
    }

    private fun toggleLocationButton(show: Boolean) {
        if (show) {
            rootView?.lytActivateLocation?.visibility = View.VISIBLE
        } else {
            rootView?.lytActivateLocation?.visibility = View.GONE
        }
    }

    private fun initMap(bundle: Bundle?) {
        rootView?.map?.onCreate(bundle)
        rootView?.map?.onResume()
        try {
            MapsInitializer.initialize(activity?.applicationContext)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        rootView?.map?.getMapAsync(this)
    }

    private fun setupClusterer() {
        clusterManager = ClusterManager(activity, googleMap)
        googleMap?.setOnCameraIdleListener(clusterManager)
        googleMap?.setOnMarkerClickListener(clusterManager)
    }

    override fun onMapReady(mMap: GoogleMap?) {
        googleMap = mMap
        googleMap?.uiSettings?.isZoomControlsEnabled = true
        val wisataPos = LatLng(-7.1182675, 112.414856)
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(wisataPos, 16.0f))
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient()
            googleMap?.isMyLocationEnabled = true
        }
        setupClusterer()
//        googleMap?.setOnMarkerClickListener {
//            val id = it.tag as Int
//            return@setOnMarkerClickListener true
//        }
    }

    @Synchronized
    private fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(activity!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient!!.connect()
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 1000
        mLocationRequest!!.fastestInterval = 1000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (ContextCompat.checkSelfPermission(activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        //
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        //
    }

    override fun onLocationChanged(location: Location?) {
        mLastLocation = location
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker!!.remove()
            mCurrLocationMarker = null
        }
        //Place current location marker
        val latLng = LatLng(location!!.latitude, location.longitude)
        val markerOptions = ClusteredMarker(latLng, "Saya")
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
        //mCurrLocationMarker =
        clusterManager?.addItem(markerOptions)

//        move map camera
        googleMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        googleMap?.animateCamera(CameraUpdateFactory.zoomTo(16f))

        requestNearMe(location)

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        }
    }

    fun requestNearMe(location: Location) {
        val rest = RestAdapter.createAPI().getNearby(location.latitude.toString(), location.longitude.toString())
        rest.enqueue(object : Callback<CallbackWisata> {
            override fun onFailure(call: Call<CallbackWisata>?, t: Throwable?) {
                // can't connect to server or STATUS is not 200
                t?.printStackTrace()
            }

            override fun onResponse(call: Call<CallbackWisata>?, response: Response<CallbackWisata>?) {
                val resp = response?.body()
                resp?.let {
                    if (it.status!!) {
                        // data received
                        displayMarkersData(it.data)
                    } else {
                        // server error happen
//                        displayError()
                    }
                }
                if (resp == null) {
                    // no data sent
                }
            }
        })
    }

    private fun displayMarkersData(items: ArrayList<WisataComplete>) {
        items.onEach {
            clusterManager?.addItem(ClusteredMarker(LatLng(it.latitude ?: 0.0, it.longitude
                    ?: 0.0), it.nama!!))
            return@onEach
        }
    }

    fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            } else {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            }
            return false
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        googleMap?.isMyLocationEnabled = true
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient()
                        }
                        toggleLocationButton(false)
                        checkGpsStatus()
                    }
                } else {
                    Toast.makeText(activity, "Permission denied", Toast.LENGTH_LONG).show()
                    rootView?.lytActivateLocation?.visibility = View.VISIBLE
                }
                return
            }
        }
    }

    private fun checkGpsStatus() {
        val locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(activity!!)
            builder
                    .setMessage("Anda belum mengaktifkan GPS, Apakah Anda ingin mengaktifkannya ?")
                    .setTitle("GPS Tidak Aktif")
                    .setCancelable(false)
                    .setPositiveButton("Ya", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            activity!!.startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                        }
                    })
                    .setNegativeButton("Tidak", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            dialog?.cancel()
                        }
                    })
            builder.create().show()
        }
    }

    private fun actionSearch(qry: String) {
        val options = arrayListOf<String>()
        qryOptions[rootView?.radioGroupJarak?.checkedRadioButtonId]?.let {
            options.add(it)
        }
        qryOptions[rootView?.radioGroupHarga?.checkedRadioButtonId]?.let {
            options.add(it)
        }
        if (checkedFasilitas.isNotEmpty()) {
            val checked = checkedFasilitas.sorted().joinToString(",")
            options.add("fas:$checked")
        }
        val opt = options.joinToString()
//        rootView?.expandableLayout?.collapse(true)
        Tools.hideKeyboard(activity!!.baseContext, rootView?.edtSearch!!)
        resetRadio()
//        Toast.makeText(context!!, opt, Toast.LENGTH_SHORT).show()
        ActivitySearchResult.navigate(activity!!, options = opt, qry = qry, location = mLastLocation)
    }

    private fun resetRadio() {
        rootView?.radioGroupJarak?.clearCheck()
        rootView?.radioGroupHarga?.clearCheck()
        arrfas?.let {
            adapterCheckFasilitas.setItems(it)
        }
    }

    private fun requestData() {
        rootView?.lytWisataPopuler?.visibility = View.GONE
        val rest = RestAdapter.createAPI().getMostRated()
        rest.enqueue(object : Callback<CallbackWisata> {
            override fun onFailure(call: Call<CallbackWisata>?, t: Throwable?) {
                // can't connect to server or STATUS is not 200
                displayError()
            }

            override fun onResponse(call: Call<CallbackWisata>?, response: Response<CallbackWisata>?) {
                val resp = response?.body()
                resp?.let {
                    if (it.status!!) {
                        // data received
                        displayData(it.data)
                    } else {
                        // server error happen
                        displayError()
                    }
                }
                if (resp == null) {
                    // no data sent
                }
            }
        })
    }

    private fun displayError() {
        rootView?.lytWisataPopuler?.visibility = View.GONE
    }

    private fun displayData(items: ArrayList<WisataComplete>) {
        rootView?.lytWisataPopuler?.visibility = View.VISIBLE
        adapter?.setItems(items)
        rootView?.recyclerView?.adapter = adapter
        adapter?.setOnItemClickListener(this)
    }

    override fun onItemClick(view: View, wisata: WisataComplete) {
        ActivityDetailWisata.navigate(activity!!, wisata)
        Toast.makeText(context, wisata.nama, Toast.LENGTH_SHORT).show()
    }

    private fun getFasilitas() {
        val callCallbackFasilitas = RestAdapter.createAPI().getFasilitas()
        callCallbackFasilitas.enqueue(object : Callback<CallbackFasilitas> {
            override fun onFailure(call: Call<CallbackFasilitas>?, t: Throwable?) {
                //
            }

            override fun onResponse(call: Call<CallbackFasilitas>?, response: Response<CallbackFasilitas>?) {
                val resp = response?.body()
                resp?.let { ti ->
                    if (ti.status!!) {
                        arrfas = ti.data
                        adapterCheckFasilitas.setItems(ti.data)
                    }
                }
            }
        })
    }

    override fun onCheckboxClick(view: View, fasilitas: Fasilitas) {
        val checkBox = view as CheckBox
        if (checkBox.isChecked)
            checkedFasilitas.add(fasilitas.id!!)
        else
            checkedFasilitas.remove(fasilitas.id)
    }

    companion object {
        private const val SEARCH_RECOMMENDED = 0
        private const val SEARCH_JARAK = 1
        private const val SEARCH_HARGA = 2
        const val MY_PERMISSIONS_REQUEST_LOCATION = 99
    }
}