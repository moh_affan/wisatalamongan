package com.wisata.app.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.DefaultSliderView
import com.wisata.app.R
import com.wisata.app.connection.RestAdapter
import com.wisata.app.connection.callbacks.CallbackSlider
import com.wisata.app.data.Constant
import kotlinx.android.synthetic.main.frame_beranda.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentBeranda : Fragment() {

    private var rootView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.frame_beranda, null)
            initComponent()
            initFragment()
        }
        return rootView
    }

    private fun initFragment() {
        val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
//        fragmentTransaction?.replace(R.id.frameRating, FragmentRating())
        fragmentTransaction?.replace(R.id.frameAlam, FragmentAlam())
        fragmentTransaction?.replace(R.id.frameBudaya, FragmentBudaya())
        fragmentTransaction?.replace(R.id.frameReligi, FragmentReligi())
        fragmentTransaction?.replace(R.id.frameKuliner, FragmentKuliner())
        fragmentTransaction?.commit()
    }

    private fun showDefaultSlider() {
        val sld1 = DefaultSliderView(activity)
                .setScaleType(BaseSliderView.ScaleType.Fit)
                .image("http://3.bp.blogspot.com/-qqRVTOlaJ5k/Vee37s2a9pI/AAAAAAAAACo/7e6HjqErBdI/s1600/tari%2Bsilir.jpg")
                .empty(R.drawable.loading_placeholder)
                .error(R.drawable.no_image)
        rootView?.sliderLayout?.addSlider(sld1)
    }


    private fun initComponent() {
        val callbackSlider = RestAdapter.createAPI().getListSlider()
        callbackSlider.enqueue(object : Callback<CallbackSlider> {
            override fun onFailure(call: Call<CallbackSlider>?, t: Throwable?) {
                showDefaultSlider()
            }

            override fun onResponse(call: Call<CallbackSlider>?, response: Response<CallbackSlider>?) {
                val resp = response?.body()
                if (resp == null)
                    showDefaultSlider()
                resp?.let {
                    if (it.status!!) {
                        it.data.onEach {
                            val sld = DefaultSliderView(activity)
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .image(Constant.getUrlSliderImg(it.namaFile!!))
                                    .empty(R.drawable.loading_placeholder)
                                    .error(R.drawable.no_image)
                            rootView?.sliderLayout?.addSlider(sld)
//                            Log.d("img-url", Constant.getUrlSliderImg(it.namaFile!!))
                        }
                    }
                }
            }
        })
    }
}