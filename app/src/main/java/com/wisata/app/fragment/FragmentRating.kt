package com.wisata.app.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.wisata.app.R
import com.wisata.app.adapter.AdapterWisata
import com.wisata.app.connection.RestAdapter
import com.wisata.app.connection.callbacks.CallbackWisata
import com.wisata.app.model.WisataComplete
import kotlinx.android.synthetic.main.frame_rating.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentRating : Fragment(), AdapterWisata.OnItemClickListener {
    private var rootView: View? = null
    private var adapter: AdapterWisata? = null
    private var items: ArrayList<WisataComplete> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.frame_rating, null)
            initComponent()
            requestData()
        }
        return rootView
    }

    private fun initComponent() {
        rootView?.recyclerView?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        adapter = AdapterWisata(context!!, items)
        rootView?.btnTryAgain?.setOnClickListener { requestData() }
    }

    private fun requestData() {
        rootView?.progressBar?.visibility = View.VISIBLE
        rootView?.recyclerView?.visibility = View.GONE
        rootView?.layoutTryAgain?.visibility = View.GONE
        val rest = RestAdapter.createAPI().getWisata()
        rest.enqueue(object : Callback<CallbackWisata> {
            override fun onFailure(call: Call<CallbackWisata>?, t: Throwable?) {
                // can't connect to server or STATUS is not 200
                displayError()
            }

            override fun onResponse(call: Call<CallbackWisata>?, response: Response<CallbackWisata>?) {
                val resp = response?.body()
                resp?.let {
                    if (it.status!!) {
                        // data received
                        displayData(it.data)
                    } else {
                        // server error happen
                        displayError()
                    }
                }
                if (resp == null) {
                    // no data sent
                }
            }
        })
    }

    private fun displayError() {
        rootView?.layoutTryAgain?.visibility = View.VISIBLE
        rootView?.recyclerView?.visibility = View.GONE
        rootView?.progressBar?.visibility = View.GONE
    }

    private fun displayData(items: ArrayList<WisataComplete>) {
        rootView?.recyclerView?.visibility = View.VISIBLE
        rootView?.progressBar?.visibility = View.GONE
        rootView?.layoutTryAgain?.visibility = View.GONE
        adapter?.setItems(items)
        rootView?.recyclerView?.adapter = adapter
        adapter?.setOnItemClickListener(this)
    }

    override fun onItemClick(view: View, wisata: WisataComplete) {
        Toast.makeText(context, wisata.nama, Toast.LENGTH_SHORT).show()
    }
}