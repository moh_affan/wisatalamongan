package com.wisata.app.widget

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class ClusteredMarker(position: LatLng, title: String) : ClusterItem {
    private var position: LatLng? = null
    private var title: String? = null
    private var snippet: String? = null

    init {
        this.position = position
        this.title = title
    }

    constructor(position: LatLng, title: String, snippet: String) : this(position, title) {
        this.snippet = snippet
    }

    override fun getSnippet(): String? {
        return snippet
    }

    override fun getTitle(): String? {
        return title
    }

    override fun getPosition(): LatLng? {
        return position
    }
}