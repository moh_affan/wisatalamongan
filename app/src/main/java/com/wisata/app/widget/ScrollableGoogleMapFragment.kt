package com.wisata.app.widget

import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.gms.maps.SupportMapFragment

class ScrollableGoogleMapFragment : SupportMapFragment() {
    var mListener: OnTouchListener? = null

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstance: Bundle?): View? {
        val layout = super.onCreateView(inflater, parent, savedInstance)
        val frameLyt = TouchableWrapper(activity!!)
        frameLyt.setBackgroundColor(ContextCompat.getColor(activity!!, android.R.color.transparent))
        (layout as ViewGroup).addView(frameLyt, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        return layout
    }

    interface OnTouchListener {
        fun onTouch()
    }

    inner class TouchableWrapper(context: Context) : FrameLayout(context) {
        override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
            when (ev?.action) {
                MotionEvent.ACTION_DOWN -> mListener?.onTouch()
                MotionEvent.ACTION_UP -> mListener?.onTouch()
            }
            return super.dispatchTouchEvent(ev)
        }
    }
}