package com.wisata.app.connection.callbacks

import com.google.gson.annotations.SerializedName
import com.wisata.app.model.Fasilitas
import java.io.Serializable

class CallbackFasilitas : DefaultCallback(), Serializable {
    @SerializedName("data")
    var data: ArrayList<Fasilitas> = arrayListOf()
}