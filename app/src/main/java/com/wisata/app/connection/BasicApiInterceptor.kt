package com.wisata.app.connection

import android.util.Log
import com.wisata.app.App
import com.wisata.app.data.Constant
import com.wisata.app.data.SharedPref
import okhttp3.Interceptor
import okhttp3.Response

class BasicApiInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val authenticated = request.newBuilder()
                .addHeader("x-api-key", SharedPref(App.instance!!.applicationContext).token)
                .addHeader("User-agent", Constant.userAgent)
                .build()
        Log.d("token", SharedPref(App.instance!!.applicationContext).token)
        return chain.proceed(authenticated)
    }
}