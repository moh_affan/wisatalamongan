package com.wisata.app.connection.callbacks

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class DefaultCallback : Serializable {
    @SerializedName("status")
    var status: Boolean? = true
    @SerializedName("error")
    var error: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("msg")
    var msg: String? = null
}