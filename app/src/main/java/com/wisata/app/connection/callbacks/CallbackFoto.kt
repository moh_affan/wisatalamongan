package com.wisata.app.connection.callbacks

import com.google.gson.annotations.SerializedName
import com.wisata.app.model.Foto
import java.io.Serializable

class CallbackFoto : DefaultCallback(), Serializable {
    @SerializedName("data")
    var data: ArrayList<Foto> = arrayListOf()
}