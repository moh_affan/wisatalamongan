package com.wisata.app.connection.callbacks

import com.google.gson.annotations.SerializedName
import com.wisata.app.model.WisataComplete
import java.io.Serializable

class CallbackSingleWisata : DefaultCallback(), Serializable {
    @SerializedName("data")
    var data: WisataComplete? = null
}