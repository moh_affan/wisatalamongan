package com.wisata.app.connection

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response

class BasicAuthInterceptor(username: String, password: String) : Interceptor {
    private var credential: String? = null

    init {
        this.credential = Credentials.basic(username, password)
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val authenticated = request.newBuilder().header("Authorization", this.credential!!).build()
        return chain.proceed(authenticated)
    }
}