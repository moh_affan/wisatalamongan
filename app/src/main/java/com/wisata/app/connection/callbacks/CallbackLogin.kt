package com.wisata.app.connection.callbacks

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CallbackLogin : DefaultCallback(), Serializable {
    @SerializedName("data")
    var apikey: String = "11111"
}