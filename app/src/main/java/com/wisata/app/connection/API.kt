package com.wisata.app.connection

import com.wisata.app.connection.callbacks.*
import retrofit2.Call
import retrofit2.http.*

interface API {

    @GET("main/single_wisata")
    fun getSingleWisata(@Query("id_wisata") idWisata: Int): Call<CallbackSingleWisata>

    @GET("main/wisata")
    fun getWisata(@Query("per_page") perPage: Int = 10,
                  @Query("page_number") pageNumber: Int = 1,
                  @Query("jenis_id") jenisId: String = "",
                  @Query("qry") qry: String = "",
                  @Query("field_name") fieldName: String = ""
    ): Call<CallbackWisata>

    @GET("main/qry_wisata")
    fun getQueryWisata(@Query("options") options: String = "",
                       @Query("latitude") latitude: String = "",
                       @Query("longitude") longitude: String = "",
                       @Query("qry") qry: String = ""
    ): Call<CallbackWisata>

    @GET("main/nearby")
    fun getNearby(@Query("latitude") latitude: String,
                  @Query("longitude") longitude: String): Call<CallbackWisata>

    @GET("main/most_rated")
    fun getMostRated(): Call<CallbackWisata>

    @GET("main/fasilitas")
    fun getFasilitas(): Call<CallbackFasilitas>

    @GET("main/foto")
    fun getListFoto(@Query("wisata_id") wisataId: Int): Call<CallbackFoto>

    @GET("main/slider")
    fun getListSlider(): Call<CallbackSlider>

    @GET("main/rating")
    fun getRating(@Query("wisata_id") wisataId: Int, @Query("user_id") userId: String = ""): Call<CallbackRatingUser>

    @FormUrlEncoded
    @POST("main/rate")
    fun postRating(@Field("user_id") userId: String,
                   @Field("wisata_id") wisataId: String,
                   @Field("rating") rating: Float,
                   @Field("komentar") komentar: String
    ): Call<DefaultCallback>

    @FormUrlEncoded
    @POST("auth/register")
    fun registerUser(@Field("username") username: String,
                     @Field("password") password: String,
                     @Field("email") email: String,
                     @Field("first_name") nama: String,
                     @Field("last_name") userType: String,
                     @Field("company") company: String,
                     @Field("phone") phone: String
    ): Call<DefaultCallback>

    @FormUrlEncoded
    @POST("auth/login")
    fun submitLogin(@Field("identity") username: String,
                    @Field("password") password: String
    ): Call<CallbackLogin>

    @POST("auth/logout")
    fun doLogout(): Call<DefaultCallback>

    @GET("auth/profil")
    fun getProfil(): Call<CallbackProfil>

    @GET("user/wisata")
    fun getUserWisata(@Query("per_page") perPage: Int = 10,
                      @Query("page_number") pageNumber: Int = 1,
                      @Query("user_id") userId: String = "",
                      @Query("qry") qry: String = "",
                      @Query("field_name") fieldName: String = ""
    ): Call<CallbackWisata>

    @FormUrlEncoded
    @POST("user/store")
    fun postWisata(@Field("nama_wisata") namaWisata: String,
                   @Field("jenis_id") jenisId: String,
                   @Field("cost") cost: String,
                   @Field("luas") luas: String,
                   @Field("deskripsi") deskripsi: String,
                   @Field("latitude") latitude: String,
                   @Field("longitude") longitude: String,
                   @Field("fasilitas_id") fasilitas: String,
                   @Field("foto") foto: String,
                   @Field("edit") edit: String = "0"
    ): Call<DefaultCallback>

    @FormUrlEncoded
    @POST("user/store")
    fun editWisata(@Field("nama_wisata") namaWisata: String,
                   @Field("jenis_id") jenisId: String,
                   @Field("cost") cost: String,
                   @Field("luas") luas: String,
                   @Field("deskripsi") deskripsi: String,
                   @Field("latitude") latitude: String,
                   @Field("longitude") longitude: String,
                   @Field("fasilitas_id") fasilitas: String,
                   @Field("foto") foto: String,
                   @Field("edit") edit: String = "1"
    ): Call<DefaultCallback>

    @FormUrlEncoded
    @POST("user/remove")
    fun removeWisata(@Field("id_wisata") idWisata: String): Call<DefaultCallback>
}
