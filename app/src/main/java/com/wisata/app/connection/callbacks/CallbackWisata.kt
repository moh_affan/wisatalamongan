package com.wisata.app.connection.callbacks

import com.google.gson.annotations.SerializedName
import com.wisata.app.model.WisataComplete
import java.io.Serializable

class CallbackWisata : DefaultCallback(), Serializable {
    @SerializedName("data")
    var data: ArrayList<WisataComplete> = arrayListOf()
    @SerializedName("total_items")
    var totalItems: Int? = null
}