package com.wisata.app.connection.callbacks

import com.google.gson.annotations.SerializedName
import com.wisata.app.model.User
import java.io.Serializable

class CallbackProfil : DefaultCallback(), Serializable {
    @SerializedName("data")
    var data: User? = null
}