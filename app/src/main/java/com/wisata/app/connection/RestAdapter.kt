package com.wisata.app.connection

import com.wisata.app.App
import com.wisata.app.BuildConfig
//import com.wisata.app.BuildConfig
import com.wisata.app.data.Constant
import okhttp3.Cache
import okhttp3.OkHttpClient.Builder
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RestAdapter {
    fun createAPI(): API {
        val logging = HttpLoggingInterceptor()
        logging.level = if (BuildConfig.BUILD_TYPE == "debug") Level.BODY else Level.NONE
        val builder = Builder()
        builder.connectTimeout(5, TimeUnit.SECONDS)
        builder.writeTimeout(10, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)
        builder.addInterceptor(logging)
        builder.addInterceptor(BasicAuthInterceptor("mimin", "1111"))
        builder.addInterceptor(BasicApiInterceptor())
//        builder.cache(null)
        builder.cache(Cache(App.instance!!.cacheDir, 2400))
        return Retrofit.Builder().baseUrl(Constant.API_URL).addConverterFactory(GsonConverterFactory.create()).client(builder.build()).build().create(API::class.java) as API
    }
}
