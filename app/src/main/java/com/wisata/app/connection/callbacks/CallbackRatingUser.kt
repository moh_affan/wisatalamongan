package com.wisata.app.connection.callbacks

import com.google.gson.annotations.SerializedName
import com.wisata.app.model.RatingUser
import java.io.Serializable

class CallbackRatingUser : DefaultCallback(), Serializable {
    @SerializedName("data")
    var data: ArrayList<RatingUser>? = arrayListOf()
}