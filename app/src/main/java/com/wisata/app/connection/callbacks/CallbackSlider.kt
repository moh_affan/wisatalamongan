package com.wisata.app.connection.callbacks

import com.google.gson.annotations.SerializedName
import com.wisata.app.model.Slider
import java.io.Serializable

class CallbackSlider : DefaultCallback(), Serializable {
    @SerializedName("data")
    var data: ArrayList<Slider> = arrayListOf()
}