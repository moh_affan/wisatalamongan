package com.wisata.app.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wisata.app.R
import com.wisata.app.model.Fasilitas
import kotlinx.android.synthetic.main.item_fasilitas.view.*
import java.util.*

class AdapterFasilitas(private val ctx: Context, items: List<Fasilitas>) : Adapter<android.support.v7.widget.RecyclerView.ViewHolder>() {
    private var items: List<Fasilitas> = ArrayList()
    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(view: View, fasilitas: Fasilitas)
    }

    inner class ViewHolder(v: View) : android.support.v7.widget.RecyclerView.ViewHolder(v)

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    init {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): android.support.v7.widget.RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_fasilitas, parent, false))
    }

    override fun onBindViewHolder(holder: android.support.v7.widget.RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val c = this.items[position]
            holder.itemView.txtItemFasilitas.text = c.nama
            holder.itemView.lyt_parent.setOnClickListener { v ->
                if (this@AdapterFasilitas.onItemClickListener != null) {
                    this@AdapterFasilitas.onItemClickListener!!.onItemClick(v, c)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun setItems(items: List<Fasilitas>) {
        this.items = items
        notifyDataSetChanged()
    }
}
