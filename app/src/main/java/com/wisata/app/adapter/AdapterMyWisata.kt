package com.wisata.app.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.*
import android.support.v7.widget.RecyclerView.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wisata.app.R
import com.wisata.app.data.Constant
import com.wisata.app.data.SharedPref
import com.wisata.app.model.WisataComplete
import com.wisata.app.utils.Tools
import kotlinx.android.synthetic.main.item_loading.view.*
import kotlinx.android.synthetic.main.item_my_wisata.view.*
import java.util.*

class AdapterMyWisata(private val ctx: Context, view: RecyclerView, items: MutableList<WisataComplete?>) : Adapter<ViewHolder>() {
    private val VIEW_ITEM = 1
    private val VIEW_PROG = 0
    private var items: MutableList<WisataComplete?> = ArrayList()
    private var loading: Boolean = false
    private var mOnItemClickListener: OnItemClickListener? = null
    private var onLoadMoreListener: OnLoadMoreListener? = null
    private var onPopupItemClickListener: OnPopupItemClickListener? = null
    private val sharedPref: SharedPref

    private var activity: AppCompatActivity? = null

    interface OnItemClickListener {
        fun onItemClick(view: View, wisata: WisataComplete, i: Int)
    }

    interface OnPopupItemClickListener {
        fun onEditItemClick(view: View, wisata: WisataComplete, i: Int)
        fun onDeleteItemClick(view: View, wisata: WisataComplete, i: Int)
    }

    interface OnLoadMoreListener {
        fun onLoadMore(i: Int)
    }

    inner class OriginalViewHolder(v: View) : ViewHolder(v)

    class ProgressViewHolder(v: View) : ViewHolder(v)

    fun setOnPopupItemClickListener(mOnPopupItemClickListener: OnPopupItemClickListener) {
        this.onPopupItemClickListener = mOnPopupItemClickListener
    }

    fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
        this.mOnItemClickListener = mItemClickListener
    }

    init {
        this.items = items
        this.sharedPref = SharedPref(this.ctx)
        lastItemViewDetector(view)
        view.addItemDecoration(DividerItemDecoration(view.context, RecyclerView.VERTICAL))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == VIEW_ITEM) {
            OriginalViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_my_wisata, parent, false))
        } else ProgressViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder is OriginalViewHolder) {
            val c = this.items[position]
            holder.itemView.nama.text = c?.nama
            c?.foto?.get(0)?.let {
                Tools.displayImageThumbnail(this.ctx, holder.itemView.image, Constant.getUrlImg(it.id!!), 0.5f)
                return@let
            }
            c?.rating?.let {
                if (it.isNotEmpty()) {
                    val rat = Tools.avgRating(it)
                    holder.itemView.ratingBar.rating = rat
                    holder.itemView.txtRating.text = "$rat"
                }
            }
            holder.itemView.btnPopup?.setOnClickListener {
                showPopup(it, c!!, position)
            }
            c?.approved?.let {
                if (it == 0) {
                    holder.itemView.txtApproved?.setTextColor(ContextCompat.getColor(activity!!, R.color.red_400))
                    holder.itemView.txtApproved?.text = "Ditolak"
                } else if (it == 1) {
                    holder.itemView.txtApproved?.setTextColor(ContextCompat.getColor(activity!!, R.color.green_400))
                    holder.itemView.txtApproved?.text = "Diterima"
                    holder.itemView.btnPopup?.visibility = View.GONE
                } else {
                    holder.itemView.txtApproved?.setTextColor(ContextCompat.getColor(activity!!, R.color.amber_700))
                    holder.itemView.txtApproved?.text = "Belum Diterima"
                }
                return@let
            }
            holder.itemView.lyt_parent.setOnClickListener { v ->
                if (this@AdapterMyWisata.mOnItemClickListener != null) {
                    this@AdapterMyWisata.mOnItemClickListener!!.onItemClick(v, c!!, position)
                }
            }
            return
        }
        (holder as ProgressViewHolder).itemView.progress_loading.isIndeterminate = true
    }

    private fun showPopup(view: View, wisata: WisataComplete, index: Int) {
        val popup = PopupMenu(activity!!, view)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.wisata_action, popup.menu)
        popup.setOnMenuItemClickListener {
            if (onPopupItemClickListener != null) {
                when (it.itemId) {
                    R.id.action_edit -> {
                        onPopupItemClickListener?.onEditItemClick(view, wisata, index)
                        return@setOnMenuItemClickListener true
                    }
                    R.id.action_delete -> {
                        onPopupItemClickListener?.onDeleteItemClick(view, wisata, index)
                        return@setOnMenuItemClickListener true
                    }
                }
            }
            return@setOnMenuItemClickListener false
        }
        popup.show()
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (this.items[position] != null) VIEW_ITEM else VIEW_PROG
    }

    fun insertData(items: List<WisataComplete>) {
        setLoaded()
        val positionStart = itemCount
        val itemCount = items.size
        this.items.addAll(items)
        notifyItemRangeInserted(positionStart, itemCount)
    }

    fun setLoaded() {
        this.loading = false
        for (i in 0 until itemCount) {
            if (this.items[i] == null) {
                this.items.removeAt(i)
                notifyItemRemoved(i)
            }
        }
    }

    fun deleteAt(position: Int) {
        this.items.removeAt(position)
        notifyItemRemoved(position)

    }

    fun setLoading() {
        if (itemCount != 0) {
            this.items.add(null)
            notifyItemInserted(itemCount - 1)
            this.loading = true
        }
    }

    fun resetListData() {
        this.items = ArrayList()
        notifyDataSetChanged()
    }

    fun setOnLoadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

    fun setActivity(activity: AppCompatActivity) {
        this.activity = activity
    }

    private fun lastItemViewDetector(recyclerView: RecyclerView) {
        val layoutManager = if (recyclerView.layoutManager is LinearLayoutManager) recyclerView.layoutManager as LinearLayoutManager else recyclerView.layoutManager as GridLayoutManager
        recyclerView.addOnScrollListener(object : OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastPos = layoutManager.findLastVisibleItemPosition()
                if (!this@AdapterMyWisata.loading && lastPos == this@AdapterMyWisata.itemCount - 1 && this@AdapterMyWisata.onLoadMoreListener != null) {
                    if (this@AdapterMyWisata.onLoadMoreListener != null) {
                        this@AdapterMyWisata.onLoadMoreListener!!.onLoadMore(Math.ceil(this@AdapterMyWisata.itemCount / Constant.ITEMS_PER_REQUEST.toDouble()).toInt() - 1)
                    }
                    this@AdapterMyWisata.loading = true
                }
            }
        })
        if (layoutManager is GridLayoutManager) {
            layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    when (recyclerView.adapter.getItemViewType(position)) {
                        VIEW_PROG -> return Tools.getGridSpanCount(activity!!)
                        VIEW_ITEM -> return 1
                        else -> return -1
                    }
                }
            }
        }
    }
}
