package com.wisata.app.adapter

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wisata.app.R
import com.wisata.app.data.Constant
import com.wisata.app.data.SharedPref
import com.wisata.app.model.WisataComplete
import com.wisata.app.utils.Tools
import kotlinx.android.synthetic.main.item_loading.view.*
import kotlinx.android.synthetic.main.item_wisata.view.*
import java.util.*

class AdapterPaginatedWisata(private val ctx: Context, view: RecyclerView, items: MutableList<WisataComplete?>) : Adapter<ViewHolder>() {
    private val VIEW_ITEM = 1
    private val VIEW_PROG = 0
    private var items: MutableList<WisataComplete?> = ArrayList()
    private var loading: Boolean = false
    private var mOnItemClickListener: OnItemClickListener? = null
    private var onLoadMoreListener: OnLoadMoreListener? = null
    private val sharedPref: SharedPref

    private var activity: AppCompatActivity? = null

    interface OnItemClickListener {
        fun onItemClick(view: View, wisata: WisataComplete, i: Int)
    }

    interface OnLoadMoreListener {
        fun onLoadMore(i: Int)
    }

    inner class OriginalViewHolder(v: View) : ViewHolder(v)

    class ProgressViewHolder(v: View) : ViewHolder(v)

    fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
        this.mOnItemClickListener = mItemClickListener
    }

    init {
        this.items = items
        this.sharedPref = SharedPref(this.ctx)
        lastItemViewDetector(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == VIEW_ITEM) {
            OriginalViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_wisata, parent, false))
        } else ProgressViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder is OriginalViewHolder) {
            val c = this.items[position]
            holder.itemView.nama.text = c?.nama
            c?.foto?.get(0)?.let {
                Tools.displayImageThumbnail(this.ctx, holder.itemView.image, Constant.getUrlImg(it.id!!), 0.5f)
                return@let
            }
            c?.rating?.let {
                if (it.isNotEmpty()) {
                    val rat = Tools.avgRating(it)
                    holder.itemView.ratingBar.rating = rat
                    holder.itemView.txtRating.text = "$rat"
                }
            }
            holder.itemView.lyt_parent.setOnClickListener { v ->
                if (this@AdapterPaginatedWisata.mOnItemClickListener != null) {
                    this@AdapterPaginatedWisata.mOnItemClickListener!!.onItemClick(v, c!!, position)
                }
            }
            return
        }
        (holder as ProgressViewHolder).itemView.progress_loading.isIndeterminate = true
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (this.items[position] != null) VIEW_ITEM else VIEW_PROG
    }

    fun insertData(items: List<WisataComplete>) {
        setLoaded()
        val positionStart = itemCount
        val itemCount = items.size
        this.items.addAll(items)
        notifyItemRangeInserted(positionStart, itemCount)
    }

    fun setLoaded() {
        this.loading = false
        for (i in 0 until itemCount) {
            if (this.items[i] == null) {
                this.items.removeAt(i)
                notifyItemRemoved(i)
            }
        }
    }

    fun setLoading() {
        if (itemCount != 0) {
            this.items.add(null)
            notifyItemInserted(itemCount - 1)
            this.loading = true
        }
    }

    fun resetListData() {
        this.items = ArrayList()
        notifyDataSetChanged()
    }

    fun setOnLoadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

    fun setActivity(activity: AppCompatActivity) {
        this.activity = activity
    }

    private fun lastItemViewDetector(recyclerView: RecyclerView) {
        val layoutManager = if (recyclerView.layoutManager is LinearLayoutManager) recyclerView.layoutManager as LinearLayoutManager else recyclerView.layoutManager as GridLayoutManager
        recyclerView.addOnScrollListener(object : OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastPos = layoutManager.findLastVisibleItemPosition()
                if (!this@AdapterPaginatedWisata.loading && lastPos == this@AdapterPaginatedWisata.itemCount - 1 && this@AdapterPaginatedWisata.onLoadMoreListener != null) {
                    if (this@AdapterPaginatedWisata.onLoadMoreListener != null) {
                        this@AdapterPaginatedWisata.onLoadMoreListener!!.onLoadMore(Math.ceil(this@AdapterPaginatedWisata.itemCount / Constant.ITEMS_PER_REQUEST.toDouble()).toInt() - 1)
                    }
                    this@AdapterPaginatedWisata.loading = true
                }
            }
        })
        if (layoutManager is GridLayoutManager) {
            layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    when (recyclerView.adapter.getItemViewType(position)) {
                        VIEW_PROG -> return Tools.getGridSpanCount(activity!!)
                        VIEW_ITEM -> return 1
                        else -> return -1
                    }
                }
            }
        }
    }
}
