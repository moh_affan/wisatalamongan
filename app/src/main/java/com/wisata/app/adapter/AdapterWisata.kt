package com.wisata.app.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wisata.app.R
import com.wisata.app.data.Constant
import com.wisata.app.model.WisataComplete
import com.wisata.app.utils.Tools
import kotlinx.android.synthetic.main.item_wisata_horizontal.view.*
import java.util.*

class AdapterWisata(private val ctx: Context, items: List<WisataComplete>) : Adapter<android.support.v7.widget.RecyclerView.ViewHolder>() {
    private var items: List<WisataComplete> = ArrayList()
    private var onItemClickListener: OnItemClickListener? = null
    var isResultDisplayed = false

    interface OnItemClickListener {
        fun onItemClick(view: View, wisata: WisataComplete)
    }

    inner class ViewHolder(v: View) : android.support.v7.widget.RecyclerView.ViewHolder(v)

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    init {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): android.support.v7.widget.RecyclerView.ViewHolder {
        return if (!isResultDisplayed) ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_wisata_horizontal, parent, false)) else ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_wisata, parent, false))
    }

    override fun onBindViewHolder(holder: android.support.v7.widget.RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val c = this.items[position]
            holder.itemView.nama.text = c.nama
            c.foto?.get(0)?.let {
                Tools.displayImageThumbnail(this.ctx, holder.itemView.image, Constant.getUrlImg(it.id!!), 0.5f)
                return@let
            }
            c.rating?.let {
                if (it.isNotEmpty()) {
                    val rat = Tools.avgRating(it)
                    holder.itemView.ratingBar.rating = rat
                    holder.itemView.txtRating.text = "$rat"
                }
            }
            holder.itemView.lyt_parent.setOnClickListener { v ->
                if (this@AdapterWisata.onItemClickListener != null) {
                    this@AdapterWisata.onItemClickListener!!.onItemClick(v, c)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun setItems(items: List<WisataComplete>) {
        this.items = items
        notifyDataSetChanged()
    }
}
