package com.wisata.app.adapter

import android.content.Context
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wisata.app.R
import com.wisata.app.model.RatingUser
import kotlinx.android.synthetic.main.item_comment.view.*
import java.util.*

class AdapterComment(private val ctx: Context, recyclerView: RecyclerView, items: List<RatingUser>) : Adapter<android.support.v7.widget.RecyclerView.ViewHolder>() {
    private var items: List<RatingUser> = ArrayList()
    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(view: View, ratingUser: RatingUser)
    }

    inner class ViewHolder(v: View) : android.support.v7.widget.RecyclerView.ViewHolder(v)

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    init {
        this.items = items
        recyclerView.addItemDecoration(DividerItemDecoration(ctx, RecyclerView.VERTICAL))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): android.support.v7.widget.RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false))
    }

    override fun onBindViewHolder(holder: android.support.v7.widget.RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val c = this.items[position]
            holder.itemView.txtNama.text = if (c.user != null) c.user?.nama else "Pengguna"
            holder.itemView.txtKomen.text = c.komentar
            holder.itemView.ratingBar.rating = c.rating!!
            holder.itemView.lyt_parent.setOnClickListener { v ->
                if (this@AdapterComment.onItemClickListener != null) {
                    this@AdapterComment.onItemClickListener!!.onItemClick(v, c)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun setItems(items: List<RatingUser>) {
        this.items = items
        notifyDataSetChanged()
    }
}
