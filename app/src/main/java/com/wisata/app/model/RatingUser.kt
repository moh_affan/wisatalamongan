package com.wisata.app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RatingUser : Rating(), Serializable {
    @SerializedName("pengguna")
    var user: User? = null
}