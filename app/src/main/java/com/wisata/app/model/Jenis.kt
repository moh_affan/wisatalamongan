package com.wisata.app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Jenis : Serializable {
    @SerializedName("id_jenis")
    var id: Int? = null
    @SerializedName("nama_jenis")
    var nama: String? = null
}