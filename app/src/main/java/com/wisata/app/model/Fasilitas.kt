package com.wisata.app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Fasilitas : Serializable {
    @SerializedName("id_fasilitas")
    var id: Int? = null
    @SerializedName("nama_fasilitas")
    var nama: String? = null
    @SerializedName("deskripsi")
    var deskripsi: String? = null
}