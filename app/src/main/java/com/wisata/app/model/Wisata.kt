package com.wisata.app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class Wisata : Serializable {
    @SerializedName("id_wisata")
    var id: Int? = null
    @SerializedName("nama_wisata")
    var nama: String? = null
    @SerializedName("jenis_id")
    var jenisId: Int? = null
    @SerializedName("luas")
    var luas: Float? = null
    @SerializedName("deskripsi")
    var deskripsi: String? = null
    @SerializedName("lat")
    var latitude: Double? = null
    @SerializedName("lng")
    var longitude: Double? = null
    @SerializedName("cost")
    var biaya: Double? = null
    @SerializedName("approved")
    var approved: Int? = null
    @SerializedName("is_from_user")
    var isFromUser: Int? = null
}