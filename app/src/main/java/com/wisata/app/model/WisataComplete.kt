package com.wisata.app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class WisataComplete : Wisata(), Serializable {
    @SerializedName("jenis")
    var jenis: Jenis? = null
    @SerializedName("foto")
    var foto: ArrayList<Foto>? = arrayListOf()
    @SerializedName("fasilitas")
    var fasilitas: ArrayList<Fasilitas>? = arrayListOf()
    @SerializedName("rating")
    var rating: ArrayList<Rating>? = arrayListOf()
    @SerializedName("jarak")
    var jarak: Double? = -1.0
    @SerializedName("kualitas")
    var kualitas: Int? = -1
}