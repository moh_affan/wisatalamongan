package com.wisata.app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Foto : Serializable {
    @SerializedName("id_foto")
    var id: Int? = null
    @SerializedName("wisata_id")
    var wisataId: Int? = null
    @SerializedName("nama_foto")
    var strFoto: String? = null
}