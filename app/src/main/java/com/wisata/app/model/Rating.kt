package com.wisata.app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class Rating : Serializable {
    @SerializedName("user_id")
    var userId: Int? = null
    @SerializedName("wisata_id")
    var wisataId: Int? = null
    @SerializedName("rating")
    var rating: Float? = null
    @SerializedName("komentar")
    var komentar: String? = null
}