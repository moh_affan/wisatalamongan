package com.wisata.app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class User : Serializable {
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("username")
    var username: String? = null
    @SerializedName("ip_address")
    var ipAddress: String? = null
    @SerializedName("email")
    var email: String? = null
    @SerializedName("first_name")
    var nama: String? = null
    @SerializedName("last_name")
    var userType: String? = null
    @SerializedName("phone")
    var phone: String? = null
}