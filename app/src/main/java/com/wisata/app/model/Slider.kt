package com.wisata.app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Slider : Serializable {
    @SerializedName("id_slider")
    var idSlider: Int? = null
    @SerializedName("nama_file")
    var namaFile: String? = null
    @SerializedName("deskripsi")
    var deskripsi: String? = null
}