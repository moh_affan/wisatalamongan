package com.wisata.app.data

import android.graphics.Color
import java.util.*

object Constant {
    private val random = Random()
    var BASE_URL = "http://wisata.santridrajat.com"
    var WEB_URL = "$BASE_URL/"
    var API_URL = "$BASE_URL/api/"
    val userAgent: String = "Wisata-Lamongan"
    val GUEST_API_KEY = "11111"
    val WISATA_ALAM = 3
    val WISATA_KULINER = 4
    val WISATA_BUDAYA = 1
    val WISATA_RELIGI = 2
    val ITEMS_PER_REQUEST = 10

    fun getUrlImg(id_foto: Int): String {
        return "${WEB_URL}foto/get_foto/$id_foto"
    }
    fun getUrlSliderImg(nama:String): String {
        return "${WEB_URL}images/slider/$nama"
    }

    fun getRandomColor(): Int {
        return Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256))
    }
}
