package com.wisata.app.data

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.wisata.app.model.User

class SharedPref(private val ctx: Context) {
    private val sharedPreferences: SharedPreferences = ctx.getSharedPreferences("p@3r_.._f", Context.MODE_PRIVATE)
    var token: String
        get() = this.sharedPreferences.getString(TOKEN, Constant.GUEST_API_KEY)
        set(str) = this.sharedPreferences.edit().putString(TOKEN, str).apply()
    var isLogIn: Boolean
        get() = this.sharedPreferences.getBoolean(LOGIN, false)
        set(v) = this.sharedPreferences.edit().putBoolean(LOGIN, v).apply()
    var user: User?
        get() {
            val strUsr = this.sharedPreferences.getString(USER, null)
            strUsr?.let {
                return Gson().fromJson(it, User::class.java) as User
            }
            return null
        }
        set(usr) {
            usr?.let {
                val strUsr = Gson().toJson(usr)
                this.sharedPreferences.edit().putString(USER, strUsr).apply()
                return@let
            }
            if (usr == null)
                this.sharedPreferences.edit().putString(USER, null).apply()
        }

    companion object {
        private const val USER = "_.USER"
        private const val FCM_PREF_KEY = "_.FCM_PREF_KEY"
        private const val FIRST_LAUNCH = "_.FIRST_LAUNCH"
        private const val TOKEN = "_.TOKEN"
        private const val LOGIN = "_.LOGIN"
        private const val PASSWORD = "_.MEDIA-FORM"
    }
}