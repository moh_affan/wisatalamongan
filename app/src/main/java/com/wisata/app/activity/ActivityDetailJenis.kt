package com.wisata.app.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.MenuItem
import android.view.View
import com.wisata.app.R
import com.wisata.app.adapter.AdapterPaginatedWisata
import com.wisata.app.connection.RestAdapter
import com.wisata.app.connection.callbacks.CallbackWisata
import com.wisata.app.data.Constant
import com.wisata.app.model.Jenis
import com.wisata.app.model.WisataComplete
import com.wisata.app.utils.NetworkCheck
import kotlinx.android.synthetic.main.activity_detail_jenis.*
import kotlinx.android.synthetic.main.include_load_failed.*
import kotlinx.android.synthetic.main.include_no_connection.*
import kotlinx.android.synthetic.main.include_no_data.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivityDetailJenis : AppCompatActivity() {

    private var jenis: Jenis? = null
    private var failedPage = 0
    private var totalItems = 0
    private var adapter: AdapterPaginatedWisata? = null
    private var callbackCall: Call<CallbackWisata>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_jenis)
        jenis = intent.extras[EXTRA_JENIS] as Jenis?
        setupToolbar()
        initComponents()
        requestData(1)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        jenis?.let {
            collapsingToolbar?.title = it.nama
            var resId: Int? = null
            when (it.id!!) {
                Constant.WISATA_ALAM -> resId = R.drawable.wisata_alam
                Constant.WISATA_RELIGI -> resId = R.drawable.wisata_religi
                Constant.WISATA_KULINER -> resId = R.drawable.wisata_kuliner
                Constant.WISATA_BUDAYA -> resId = R.drawable.wisata_budaya
            }
            imgHeader.setImageResource(resId!!)
        }
//        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initComponents() {
        recyclerView?.layoutManager = GridLayoutManager(this, 2)
        recyclerView?.setHasFixedSize(true)
        adapter = AdapterPaginatedWisata(this, recyclerView, ArrayList())
        recyclerView?.adapter = adapter
        adapter?.setActivity(this)
        adapter?.setOnLoadMoreListener(object : AdapterPaginatedWisata.OnLoadMoreListener {
            override fun onLoadMore(i: Int) {
                if (this@ActivityDetailJenis.totalItems <= this@ActivityDetailJenis.adapter!!.itemCount || i == 0) {
                    this@ActivityDetailJenis.adapter!!.setLoaded()
                    return
                }
                this@ActivityDetailJenis.requestData(i + 1)
            }
        })
        adapter?.setOnItemClickListener(object : AdapterPaginatedWisata.OnItemClickListener {
            override fun onItemClick(view: View, wisata: WisataComplete, i: Int) {
                ActivityDetailWisata.navigate(this@ActivityDetailJenis, wisata)
            }
        })
        swipeLayout.setOnRefreshListener {
            if (this@ActivityDetailJenis.callbackCall != null && this@ActivityDetailJenis.callbackCall!!.isExecuted) {
                this@ActivityDetailJenis.callbackCall!!.cancel()
            }
            this@ActivityDetailJenis.adapter!!.resetListData()
            this@ActivityDetailJenis.requestData(1)
        }
    }

    private fun requestData(pageNumber: Int) {
        showFailedView(false)
        showNoItemView(false)
        showNoConnectionView(false)
        if (pageNumber == 1) {
            swipeProgress(true)
        } else {
            this.adapter!!.setLoading()
        }
        Handler().postDelayed({ this@ActivityDetailJenis.requestList(pageNumber) }, 1000)
    }

    private fun displayData(items: ArrayList<WisataComplete>) {
        recyclerView.visibility = View.VISIBLE
        lytNoData.visibility = View.GONE
        lytLoadFailed.visibility = View.GONE
        lytNoConnection.visibility = View.GONE
        this.adapter!!.insertData(items)
        swipeProgress(false)
        if (items.size == 0) {
            showNoItemView(true)
        }
    }

    private fun requestList(pageNumber: Int) {
        this.callbackCall = RestAdapter.createAPI().getWisata(pageNumber = pageNumber, perPage = Constant.ITEMS_PER_REQUEST, jenisId = jenis?.id.toString())
        this.callbackCall!!.enqueue(object : Callback<CallbackWisata> {
            override fun onResponse(call: Call<CallbackWisata>, response: Response<CallbackWisata>) {
                val resp = response.body()
                if (resp == null || !resp.status!!) {
                    this@ActivityDetailJenis.onFailRequest(pageNumber)
                    return
                }
                this@ActivityDetailJenis.totalItems = resp.totalItems!!
                this@ActivityDetailJenis.displayData(resp.data)
            }

            override fun onFailure(call: Call<CallbackWisata>, t: Throwable) {
                if (!call.isCanceled) {
                    this@ActivityDetailJenis.onFailRequest(pageNumber)
                }
            }
        })
    }

    private fun onFailRequest(pageNumber: Int) {
        this.failedPage = pageNumber
        this.adapter!!.setLoaded()
        swipeProgress(false)
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true)
        } else {
            showNoConnectionView(true)
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        swipeProgress(false)
        if (this.callbackCall != null && this.callbackCall!!.isExecuted) {
            this.callbackCall!!.cancel()
        }
    }

    private fun showFailedView(show: Boolean) {
        if (show) {
            recyclerView.visibility = View.GONE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.VISIBLE
            lytNoConnection.visibility = View.GONE
        } else {
            recyclerView.visibility = View.VISIBLE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        }
        btnRetry.setOnClickListener { this@ActivityDetailJenis.requestData(this@ActivityDetailJenis.failedPage) }
    }

    private fun showNoConnectionView(show: Boolean) {
        if (show) {
            recyclerView.visibility = View.GONE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        }
        btnConnectRetry.setOnClickListener { this@ActivityDetailJenis.requestData(this@ActivityDetailJenis.failedPage) }
    }

    private fun showNoItemView(show: Boolean) {
        if (show) {
            recyclerView.visibility = View.GONE
            lytNoData.visibility = View.VISIBLE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        } else {
            recyclerView.visibility = View.VISIBLE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        }
        btnReloadData.setOnClickListener { this@ActivityDetailJenis.requestData(1) }
    }

    private fun swipeProgress(show: Boolean) {
        if (show) {
            swipeLayout.post { this@ActivityDetailJenis.swipeLayout.isRefreshing = show }
        } else {
            swipeLayout.isRefreshing = show
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val EXTRA_JENIS = "EXTRA_JENIS"
//        fun navigate(activity: AppCompatActivity, jenis: Jenis) {
//            val i = Intent(activity, ActivityDetailJenis::class.java)
//            i.putExtra(EXTRA_JENIS, jenis)
//            activity.startActivity(i)
//        }

        fun navigate(activity: FragmentActivity, jenis: Jenis) {
            val i = Intent(activity, ActivityDetailJenis::class.java)
            i.putExtra(EXTRA_JENIS, jenis)
            activity.startActivity(i)
        }
    }
}
