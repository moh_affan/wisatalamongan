package com.wisata.app.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.wisata.app.R
import com.wisata.app.adapter.AdapterComment
import com.wisata.app.adapter.AdapterFasilitas
import com.wisata.app.connection.RestAdapter
import com.wisata.app.connection.callbacks.CallbackRatingUser
import com.wisata.app.connection.callbacks.CallbackSingleWisata
import com.wisata.app.connection.callbacks.DefaultCallback
import com.wisata.app.data.Constant
import com.wisata.app.data.SharedPref
import com.wisata.app.model.RatingUser
import com.wisata.app.model.WisataComplete
import com.wisata.app.utils.Tools
import kotlinx.android.synthetic.main.activity_detail_wisata.*
import kotlinx.android.synthetic.main.content_activity_detail_wisata.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat


class ActivityDetailWisata : AppCompatActivity(), OnMapReadyCallback {

    private fun Double?.format(): String {
        var formatted = this.toString()
        try {
            val formatter = DecimalFormat("###,###.##")
            formatted = formatter.format(this)
        } catch (ex: Exception) {

        }
        return formatted
    }

    private var wisata: WisataComplete? = null
    private var adapter: AdapterFasilitas? = null
    private var callCallbackSingleWisata: Call<CallbackSingleWisata>? = null
    private var googleMap: GoogleMap? = null
    private var adapterComment: AdapterComment? = null
    private var sharedPref: SharedPref? = null
    private var userId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_wisata)
        initToolbar()
        sharedPref = SharedPref(this)
        wisata = intent.extras[KEY_WISATA] as WisataComplete
        sharedPref?.user?.let {
            userId = it.id.toString()
            return@let
        }
        initComponent()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initComponent() {
        try {
            wisata?.foto?.get(0)?.let {
                Tools.displayImageOriginal(this, imgHeader, Constant.getUrlImg(it.id!!))
                return@let
            }
            wisata?.foto?.get(1)?.let {
                imgBeforeDesc.visibility = View.VISIBLE
                Tools.displayImageOriginal(this, imgBeforeDesc, Constant.getUrlImg(it.id!!))
                return@let
            }
            wisata?.foto?.get(2)?.let {
                imgAfterDesc.visibility = View.VISIBLE
                Tools.displayImageOriginal(this, imgAfterDesc, Constant.getUrlImg(it.id!!))
                return@let
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        supportActionBar?.title = wisata?.nama
        txtDeskripsi.text = wisata?.deskripsi
        wisata?.rating?.let {
            txtRating.text = Tools.avgRating(it).toString()
            ratingBar.rating = Tools.avgRating(it)
            txtCountRating.text = it.size.toString()
        }
        if (wisata?.jenis?.id == Constant.WISATA_KULINER) {
            txtLabelHarga.text = "Harga kuliner tertinggi : "
        }
        txtHarga.text = "Rp. ${wisata?.biaya?.format()}"
        txtLuas.text = Html.fromHtml("${wisata?.luas.toString()} km<sup>2</sup>")
        adapter = AdapterFasilitas(this@ActivityDetailWisata, arrayListOf())
        recyclerViewFasilitas.adapter = adapter
        recyclerViewFasilitas.addItemDecoration(DividerItemDecoration(this, RecyclerView.VERTICAL))
        adapterComment = AdapterComment(this@ActivityDetailWisata, recyclerViewComment, arrayListOf())
        recyclerViewComment.adapter = adapterComment
        requestWisata(wisata?.id!!)
        initMap()
        btnSend.setOnClickListener {
            val r = actionRating.rating
            val k = edtComment.text.toString()
            sendRating(r, k)
        }

        if (TextUtils.isEmpty(userId)) {
            lytInpRating.visibility = View.GONE
        }
    }

    private fun requestWisata(id: Int) {
        this.callCallbackSingleWisata = RestAdapter.createAPI().getSingleWisata(id)
        this.callCallbackSingleWisata?.enqueue(object : Callback<CallbackSingleWisata> {
            override fun onFailure(call: Call<CallbackSingleWisata>?, t: Throwable?) {
                recyclerViewFasilitas.visibility = View.GONE
                txtNoFasilitas.visibility = View.VISIBLE
            }

            override fun onResponse(call: Call<CallbackSingleWisata>?, response: Response<CallbackSingleWisata>?) {
                val resp = response?.body()
                if (resp != null) {
                    if (resp.status!!) {
                        resp.data?.fasilitas?.let {
                            if (it.isEmpty()) {
                                recyclerViewFasilitas.visibility = View.GONE
                                txtNoFasilitas.visibility = View.VISIBLE
                            } else {
                                recyclerViewFasilitas.visibility = View.VISIBLE
                                txtNoFasilitas.visibility = View.GONE
                                adapter?.setItems(it)
                            }
                            return@let
                        }
                        resp.data?.rating?.let {
                            txtRating.text = Tools.avgRating(it).toString()
                            ratingBar.rating = Tools.avgRating(it)
                            txtCountRating.text = it.size.toString()
                        }
                    } else {
                        recyclerViewFasilitas.visibility = View.GONE
                        txtNoFasilitas.visibility = View.VISIBLE
                    }
                } else {
                    recyclerViewFasilitas.visibility = View.GONE
                    txtNoFasilitas.visibility = View.VISIBLE
                }
            }
        })
        requestComments()
    }

    private fun requestComments() {
        val callCallbackComment = RestAdapter.createAPI().getRating(wisataId = wisata?.id!!)
        callCallbackComment.enqueue(object : Callback<CallbackRatingUser> {
            override fun onFailure(call: Call<CallbackRatingUser>?, t: Throwable?) {
                //
            }

            override fun onResponse(call: Call<CallbackRatingUser>?, response: Response<CallbackRatingUser>?) {
                val resp = response?.body()
                resp?.let {
                    if (it.status!!) {
                        adapterComment?.setItems(it.data ?: arrayListOf())
                        it.data?.forEach {
                            if (userId.equals(it.user?.id.toString())) {
                                displayMyComment(it)
                                return@forEach
                            }
                        }
                    }
                    return@let
                }
            }
        })
    }

    private fun displayMyComment(ratingUser: RatingUser) {
        txtMyComment.visibility = View.VISIBLE
        txtNamaUser.visibility = View.VISIBLE
        comment_lyt.visibility = View.GONE
        btnSend.visibility = View.GONE
        txtMyComment.text = ratingUser.komentar
        txtNamaUser.text = ratingUser.user?.nama
        actionRating.setIsIndicator(true)
        actionRating.rating = ratingUser.rating!!
    }

    fun initMap() {
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(mMap: GoogleMap?) {
        googleMap = mMap
        googleMap?.uiSettings?.isZoomControlsEnabled = true
        val wisataPos = LatLng(wisata?.latitude!!, wisata?.longitude!!)
        val marker = MarkerOptions().position(wisataPos).title(wisata?.nama).icon(BitmapDescriptorFactory.fromResource(R.drawable.img_marker))
        googleMap?.addMarker(marker)
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(wisataPos, 16.0f))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun sendRating(rating: Float, comment: String) {
        val callCallbackRate = RestAdapter.createAPI().postRating(userId = userId, wisataId = wisata?.id.toString(), rating = rating, komentar = comment)
        callCallbackRate.enqueue(object : Callback<DefaultCallback> {
            override fun onFailure(call: Call<DefaultCallback>?, t: Throwable?) {
                Toast.makeText(this@ActivityDetailWisata, "Gagal meyimpan penilaian Anda", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<DefaultCallback>?, response: Response<DefaultCallback>?) {
                val resp = response?.body()
                resp?.let {
                    if (it.status!!) {
                        Toast.makeText(this@ActivityDetailWisata, "Berhasil meyimpan penilaian Anda", Toast.LENGTH_SHORT).show()
                        requestComments()
                        requestWisata(wisata?.id!!)
                    } else
                        Toast.makeText(this@ActivityDetailWisata, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    companion object {
        private val KEY_WISATA = "_.WISATA._"

        fun navigate(activity: FragmentActivity, wisata: WisataComplete) {
            val i = Intent(activity, ActivityDetailWisata::class.java)
            i.putExtra(KEY_WISATA, wisata)
            activity.startActivity(i)
        }

        fun navigate(activity: AppCompatActivity, wisata: WisataComplete) {
            val i = Intent(activity, ActivityDetailWisata::class.java)
            i.putExtra(KEY_WISATA, wisata)
            activity.startActivity(i)
        }
    }
}
