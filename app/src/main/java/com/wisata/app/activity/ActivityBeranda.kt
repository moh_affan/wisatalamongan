package com.wisata.app.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.wisata.app.R
import com.wisata.app.adapter.ViewPagerAdapter
import com.wisata.app.fragment.FragmentBeranda
import com.wisata.app.fragment.FragmentPencarian
import com.wisata.app.fragment.FragmentProfil
import kotlinx.android.synthetic.main.activity_beranda.*
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.toolbar.*

class ActivityBeranda : AppCompatActivity() {

    var prevMenuItem: MenuItem? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                if (supportActionBar?.isShowing!!)
                    supportActionBar?.hide()
                frameContent.currentItem = PAGE_BERANDA
            }
            R.id.navigation_dashboard -> {
                if (supportActionBar?.isShowing!!)
                    supportActionBar?.hide()
                frameContent.currentItem = PAGE_PENCARIAN
            }
            R.id.navigation_notifications -> {
                if (!supportActionBar?.isShowing!!)
                    supportActionBar?.show()
                supportActionBar?.title = "Profil"
                frameContent.currentItem = PAGE_PROFIL
            }
        }
        return@OnNavigationItemSelectedListener true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beranda)
        setupToolbar()
        initComponent()
        if (supportActionBar?.isShowing!!)
            supportActionBar?.hide()
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
//        Toast.makeText(this, "onCreate",Toast.LENGTH_SHORT).show()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Beranda"
    }

    private fun initComponent() {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(FragmentBeranda())
        adapter.addFragment(FragmentPencarian())
        adapter.addFragment(FragmentProfil())
        frameContent.adapter = adapter
        frameContent.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (prevMenuItem != null) {
                    prevMenuItem?.isChecked = false
                } else {
                    navigation.menu.getItem(0).isChecked = false
                }
                navigation.menu.getItem(position).isChecked = true
                prevMenuItem = navigation.menu.getItem(position)
                if (position == PAGE_PROFIL) {
                    if (supportActionBar?.isShowing!!)
                        supportActionBar?.hide()
                }
            }
        })
    }

    companion object {
        private val PAGE_BERANDA = 0
        private val PAGE_PENCARIAN = 1
        private val PAGE_PROFIL = 2
        fun navigate(activity: AppCompatActivity) {
            val i = Intent(activity, ActivityBeranda::class.java)
            activity.startActivity(i)
        }
    }
}
