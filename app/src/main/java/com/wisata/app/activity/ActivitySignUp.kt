package com.wisata.app.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.wisata.app.R
import com.wisata.app.connection.RestAdapter
import com.wisata.app.connection.callbacks.DefaultCallback
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.content_activity_sign_up.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivitySignUp : AppCompatActivity() {

    private var isPasswordMatch = true
    private var isFormValid = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        initToolbar()
        initComponent()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Daftar"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initComponent() {
        nama.onFocusChangeListener = mOnFocusChangeListener
        username.onFocusChangeListener = mOnFocusChangeListener
        email.onFocusChangeListener = mOnFocusChangeListener
        password.onFocusChangeListener = mOnFocusChangeListener
        phone.onFocusChangeListener = mOnFocusChangeListener
        retypePassword.onFocusChangeListener = mOnFocusChangeListener
        retypePassword.addTextChangedListener(checkMatchValue)
        btnDaftar.setOnClickListener { postData() }
    }

    private fun postData() {
        val fNama = nama.text.toString()
        val fType = "USER"
        val fPhone = phone.text.toString()
        val fEmail = email.text.toString()
        val fUsername = username.text.toString()
        val fPassword = password.text.toString()
        if (!TextUtils.isEmpty(fNama) && !TextUtils.isEmpty(fPhone) && !TextUtils.isEmpty(fEmail) && !TextUtils.isEmpty(fUsername) && !TextUtils.isEmpty(fPassword)) {
            lytProgress.visibility = View.VISIBLE
            val callCallback = RestAdapter.createAPI().registerUser(username = fUsername, password = fPassword, nama = fNama, email = fEmail, phone = fPhone, company = fType, userType = fType)
            callCallback.enqueue(object : Callback<DefaultCallback> {
                override fun onFailure(call: Call<DefaultCallback>?, t: Throwable?) {
                    lytProgress.visibility = View.GONE
                }

                override fun onResponse(call: Call<DefaultCallback>?, response: Response<DefaultCallback>?) {
                    lytProgress.visibility = View.GONE
                    val resp = response?.body()
                    resp?.let {
                        if (it.status!!) {
                            Toast.makeText(this@ActivitySignUp, "Berhasil mendaftar, silahkan login", Toast.LENGTH_SHORT).show()
                            this@ActivitySignUp.onBackPressed()
                        }
                    }
                }
            })
        } else {
            lytProgress.visibility = View.GONE
            Toast.makeText(this, "Lengkapi form yang Anda isi", Toast.LENGTH_SHORT).show()
        }
    }

    private val checkMatchValue: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (!s!!.toString().equals(password.text.toString())) {
                retypePasswordLyt.error = "Password tidak cocok"
            } else {
                retypePasswordLyt.error = null
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            //
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (!s!!.equals(password.text.toString())) {
                retypePasswordLyt.error = "Password tidak cocok"
            } else {
                retypePasswordLyt.error = null
            }
        }
    }

    private val mOnFocusChangeListener: View.OnFocusChangeListener = object : View.OnFocusChangeListener {
        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            if (!hasFocus && TextUtils.isEmpty((v as TextInputEditText).text)) {
                (v.parent.parent as TextInputLayout).error = "Baris ini harus diisi"
            }
            if(hasFocus) {
                (v?.parent?.parent as TextInputLayout).error = null
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun navigate(activity: FragmentActivity) {
            val i = Intent(activity, ActivitySignUp::class.java)
            activity.startActivity(i)
        }
    }
}
