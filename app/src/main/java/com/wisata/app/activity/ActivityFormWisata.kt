package com.wisata.app.activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.wisata.app.R
import com.wisata.app.connection.RestAdapter
import com.wisata.app.connection.callbacks.CallbackFasilitas
import com.wisata.app.connection.callbacks.CallbackSingleWisata
import com.wisata.app.connection.callbacks.DefaultCallback
import com.wisata.app.data.Constant
import com.wisata.app.data.SharedPref
import com.wisata.app.model.WisataComplete
import com.wisata.app.utils.Tools
import com.wisata.app.widget.ScrollableGoogleMapFragment
import kotlinx.android.synthetic.main.activity_form_wisata.*
import kotlinx.android.synthetic.main.content_activity_form_wisata.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream

class ActivityFormWisata : AppCompatActivity(), OnMapReadyCallback {

    private var isEditMode: Boolean? = null
    private var wisata: WisataComplete? = null
    private var wisataId = "0"
    private var sharedPref: SharedPref? = null
    private var googleMap: GoogleMap? = null
    private var marker: MarkerOptions? = null
    private val arrJenis = arrayOf("Jenis Wisata", "Wisata Alam", "Wisata Budaya", "Wisata Religi", "Wisata Kuliner")
    private val mapJenis = hashMapOf(
            arrJenis[1] to Constant.WISATA_ALAM,
            arrJenis[2] to Constant.WISATA_BUDAYA,
            arrJenis[3] to Constant.WISATA_RELIGI,
            arrJenis[4] to Constant.WISATA_KULINER
    )
    private val arrFasilitas = arrayListOf<String>()
    private val mapFasilitas = hashMapOf<String, Int>()
    private val mapSelectedFasilitas = hashMapOf<String, Int>()
    private var checkedArray: BooleanArray? = null
    private var selectedJenis = ""
    private var selectedFasilitas = ""
    private var imgWisata = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_wisata)
        sharedPref = SharedPref(this)
        isEditMode = intent.extras[KEY_EDIT_MODE] as Boolean
        try {
            wisata = intent.extras[KEY_WISATA_ID] as WisataComplete
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        initToolbar()
        initComponent()
    }

    override fun onResume() {
        super.onResume()
        wisata?.let {
            wisataId = it.id.toString()
            displayWisata()
            return@let
        }
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = if (isEditMode!!) "Edit Wisata" else "Tambah Wisata"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initComponent() {
        nama.onFocusChangeListener = mOnFocusChangeListener
        harga.onFocusChangeListener = mOnFocusChangeListener
        luas.onFocusChangeListener = mOnFocusChangeListener
        deskripsi.onFocusChangeListener = mOnFocusChangeListener
        btnSubmit.setOnClickListener { postData() }
        btnJenis.setOnClickListener { dialogJenis() }
        btnFoto.setOnClickListener { pickImage() }
        initMap()
        getFasilitas()
    }

    private fun getFasilitas() {
        val callCallbackFasilitas = RestAdapter.createAPI().getFasilitas()
        callCallbackFasilitas.enqueue(object : Callback<CallbackFasilitas> {
            override fun onFailure(call: Call<CallbackFasilitas>?, t: Throwable?) {
                //
            }

            override fun onResponse(call: Call<CallbackFasilitas>?, response: Response<CallbackFasilitas>?) {
                val resp = response?.body()
                resp?.let { ti ->
                    if (ti.status!!) {
                        checkedArray = BooleanArray(ti.data.size)
                        var i = 0
                        ti.data.forEach { fa ->
                            arrFasilitas.add(fa.nama!!)
                            mapFasilitas[fa.nama!!] = fa.id!!
                            checkedArray?.set(i++, false)
                        }
                        btnFasilitas.setOnClickListener { dialogfasilitas(arrFasilitas.toTypedArray()) }
                    }
                }
            }
        })
    }

    private fun dialogJenis() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setItems(arrJenis) { dialog, which ->
            val jns = arrJenis[which]
            btnJenis.text = jns
            selectedJenis = if (which != 0) {
                btnJenis.setTextColor(Color.BLACK)
                mapJenis[jns].toString()
            } else {
                btnJenis.setTextColor(ContextCompat.getColor(this, R.color.grey_40))
                ""
            }
            dialog.dismiss()
        }
        alertDialog.create().show()
    }

    private fun dialogfasilitas(items: Array<String>) {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setMultiChoiceItems(items, checkedArray) { _, which, isChecked ->
            val fas = arrFasilitas[which]
            checkedArray?.set(which, isChecked)
            if (isChecked) {
                mapSelectedFasilitas[fas] = mapFasilitas[fas]!!
            } else {
                mapSelectedFasilitas.remove(fas)
            }
            btnFasilitas.setTextColor(Color.BLACK)
            if (mapSelectedFasilitas.isNotEmpty()) {
                btnFasilitas.setTextColor(Color.BLACK)
                btnFasilitas.text = mapSelectedFasilitas.keys.joinToString()
            } else {
                btnFasilitas.text = "Fasilitas"
                btnFasilitas.setTextColor(ContextCompat.getColor(this, R.color.grey_40))
            }
        }
        alertDialog.create().show()
    }

    private fun postData() {
        lytProgress.visibility = View.VISIBLE
        selectedFasilitas = mapSelectedFasilitas.values.joinToString(separator = ",")
        val fNama = nama.text.toString()
        val fHarga = harga.text.toString()
        val fLuas = luas.text.toString()
        val fDeskripsi = deskripsi.text.toString()
        val fLatitude = latitude.text.toString()
        val fLongitude = longitude.text.toString()

        if (!TextUtils.isEmpty(fNama) && !TextUtils.isEmpty(fHarga) && !TextUtils.isEmpty(fLuas) && !TextUtils.isEmpty(fDeskripsi) && !TextUtils.isEmpty(fLatitude) && !TextUtils.isEmpty(fLongitude) && !TextUtils.isEmpty(selectedJenis) && !TextUtils.isEmpty(selectedFasilitas) && (!imgWisata.isEmpty() || isEditMode!!)) {
            val callCallback = RestAdapter.createAPI().postWisata(fNama, selectedJenis, fHarga, fLuas, fDeskripsi, fLatitude, fLongitude, selectedFasilitas, imgWisata.joinToString(";;"), wisataId)
            callCallback.enqueue(object : Callback<DefaultCallback> {
                override fun onFailure(call: Call<DefaultCallback>?, t: Throwable?) {
                    lytProgress.visibility = View.GONE
                    Toast.makeText(this@ActivityFormWisata, "Gagal menyimpan wisata", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<DefaultCallback>?, response: Response<DefaultCallback>?) {
                    lytProgress.visibility = View.GONE
                    val resp = response?.body()
                    resp?.let {
                        if (it.status!!) {
                            Toast.makeText(this@ActivityFormWisata, "Berhasil menyimpan data wisata", Toast.LENGTH_SHORT).show()
                            this@ActivityFormWisata.onBackPressed()
                        }
                    }
                }
            })
        } else {
            lytProgress.visibility = View.GONE
            Toast.makeText(this@ActivityFormWisata, "Data yang Anda isi belum lengkap. Silahkan lengkapi data Anda", Toast.LENGTH_SHORT).show()
        }
    }

    private val mOnFocusChangeListener: View.OnFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
        if (!hasFocus && TextUtils.isEmpty((v as TextInputEditText).text)) {
            (v.parent.parent as TextInputLayout).error = "Baris ini harus diisi"
        }
        if (hasFocus) {
            (v?.parent?.parent as TextInputLayout).error = null
        }
    }

    private fun pickImage() {
//        val intent = Intent(Intent.ACTION_PICK)
//        intent.type = "image/jpg"
//        startActivityForResult(intent, REQUEST_CODE)
        ImagePicker.create(this)
                .language("id")
//                .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                .includeVideo(false) // Show video on image picker
                .toolbarDoneButtonText("SELESAI")
//	.single() // single mode
                .multi() // multi mode (default mode)
                .limit(3) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
//	.origin(images) // original selected images, used in multi mode
//	.exclude(images) // exclude anything that in image.getPath()
//	.excludeFiles(files) // same as exclude but using ArrayList<File>
//	.theme(R.style.CustomImagePickerTheme) // must inherit ef_BaseTheme. please refer to sample
                .enableLog(true) // disabling log
                .start()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val images: List<Image> = ImagePicker.getImages(data)
            var i = 0
            val imgvs = arrayListOf<ImageView>(imgSelected, imgSelected2, imgSelected3)
            images.forEach {
                val bitmap = BitmapFactory.decodeFile(it.path)
                val outStream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, outStream)
                val byteArray = outStream.toByteArray()
                if ((byteArray.size / 1024) <= 2048) {
                    imgvs[i].setImageBitmap(bitmap)
                    imgvs[i].visibility = View.VISIBLE
                    i++
                    imgWisata.add(Base64.encodeToString(byteArray, Base64.DEFAULT))
                } else {
                    Toast.makeText(this@ActivityFormWisata, "Gambar tidak dapat diproses. Ukuran melebihi 2 MB.", Toast.LENGTH_SHORT).show()
                    return@forEach
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == REQUEST_CODE) {
//            if (resultCode == RESULT_OK) {
//                val imgUri = data?.data
//                var bitmap: Bitmap? = null
//                try {
//                    bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imgUri)
//                } catch (ex: Exception) {
//                    ex.printStackTrace()
//                }
//                bitmap?.let {
//                    val outStream = ByteArrayOutputStream()
//                    it.compress(Bitmap.CompressFormat.JPEG, 80, outStream)
//                    val byteArray = outStream.toByteArray()
//                    if ((byteArray.size / 1024) <= 1024) {
//                        imgSelected.setImageURI(imgUri)
//                        imgSelected.visibility = View.VISIBLE
//                        imgWisata = Base64.encodeToString(byteArray, Base64.DEFAULT)
//                    } else {
//                        Toast.makeText(this@ActivityFormWisata, "Gambar tidak dapat diproses. Ukuran melebihi 1 MB.", Toast.LENGTH_SHORT).show()
//                    }
//                    return@let
//                }
//            }
//        }
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as ScrollableGoogleMapFragment
        mapFragment.getMapAsync(this)
        mapFragment.mListener = object : ScrollableGoogleMapFragment.OnTouchListener {
            override fun onTouch() {
                lytScrollParent.requestDisallowInterceptTouchEvent(true)
            }
        }
    }

    override fun onMapReady(mMap: GoogleMap?) {
        googleMap = mMap
        googleMap?.uiSettings?.isZoomControlsEnabled = true
        var lat = -7.1182675
        var lng = 112.414856
        wisata?.let {
            lat = it.latitude!!
            lng = it.longitude!!
            return@let
        }
        val wisataPos = LatLng(lat, lng)
        this.marker = MarkerOptions().position(wisataPos)
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(wisataPos, 16.0f))
        googleMap?.setOnCameraIdleListener {
            googleMap?.cameraPosition?.target.let {
                latitude.text = Editable.Factory().newEditable(it?.latitude?.toString())
                longitude.text = Editable.Factory().newEditable(it?.longitude?.toString())
            }
        }
    }

    private fun displayWisata() {
        nama.text = Editable.Factory().newEditable(wisata?.nama)
        btnJenis.text = wisata?.jenis?.nama
        btnJenis.setTextColor(Color.BLACK)
        selectedJenis = wisata?.jenis?.id?.toString()!!
        harga.text = Editable.Factory().newEditable(wisata?.biaya.toString())
        luas.text = Editable.Factory().newEditable(wisata?.luas.toString())
        deskripsi.text = Editable.Factory().newEditable(wisata?.deskripsi)
        val posisi = LatLng(wisata?.latitude!!, wisata?.longitude!!)
        latitude.text = Editable.Factory().newEditable(posisi.latitude.toString())
        longitude.text = Editable.Factory().newEditable(posisi.longitude.toString())
        Log.d("posisi", "${posisi.latitude}, ${posisi.longitude}")
        googleMap?.addMarker(MarkerOptions().position(posisi).title(wisata?.nama))
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(posisi, 16.0f))
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(posisi, 16.0f))
        try {
            wisata?.foto?.get(0).let {
                imgSelected.visibility = View.VISIBLE
                Tools.displayImageThumbnail(this, imgSelected, Constant.getUrlImg(it?.id!!), 0.5f)
            }
            wisata?.foto?.get(1).let {
                imgSelected2.visibility = View.VISIBLE
                Tools.displayImageThumbnail(this, imgSelected2, Constant.getUrlImg(it?.id!!), 0.5f)
            }
            wisata?.foto?.get(2).let {
                imgSelected3.visibility = View.VISIBLE
                Tools.displayImageThumbnail(this, imgSelected3, Constant.getUrlImg(it?.id!!), 0.5f)
            }
        } catch (ex:Exception){
            ex.printStackTrace()
        }
        requestWisata(wisata?.id!!)
    }

    private fun requestWisata(id: Int) {
        val callCallbackSingleWisata = RestAdapter.createAPI().getSingleWisata(id)
        callCallbackSingleWisata.enqueue(object : Callback<CallbackSingleWisata> {
            override fun onFailure(call: Call<CallbackSingleWisata>?, t: Throwable?) {
                //
            }

            override fun onResponse(call: Call<CallbackSingleWisata>?, response: Response<CallbackSingleWisata>?) {
                val resp = response?.body()
                if (resp != null) {
                    if (resp.status!!) {
                        resp.data?.fasilitas?.forEach {
                            val fas = it.nama!!
                            val isChecked = true
                            val which = arrFasilitas.indexOf(fas)
                            checkedArray?.set(which, isChecked)
                            mapSelectedFasilitas[fas] = mapFasilitas[fas]!!
                            btnFasilitas.setTextColor(Color.BLACK)
                        }
                        if (mapSelectedFasilitas.isNotEmpty()) {
                            btnFasilitas.setTextColor(Color.BLACK)
                            btnFasilitas.text = mapSelectedFasilitas.keys.joinToString()
                        } else {
                            btnFasilitas.text = "Fasilitas"
                            btnFasilitas.setTextColor(ContextCompat.getColor(this@ActivityFormWisata, R.color.grey_40))
                        }
                    }
                }
            }
        })
    }

    companion object {
        private const val KEY_EDIT_MODE = "edit_mode"
        private const val KEY_WISATA_ID = "wisata_id"
        private const val REQUEST_CODE = 900
        fun navigate(activity: FragmentActivity, isEditMode: Boolean = false, wisata: WisataComplete? = null) {
            val i = Intent(activity, ActivityFormWisata::class.java)
            i.putExtra(KEY_EDIT_MODE, isEditMode)
            i.putExtra(KEY_WISATA_ID, wisata)
            activity.startActivity(i)
        }
    }
}
