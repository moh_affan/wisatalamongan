package com.wisata.app.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.wisata.app.R
import com.wisata.app.adapter.AdapterMyWisata
import com.wisata.app.connection.RestAdapter
import com.wisata.app.connection.callbacks.CallbackWisata
import com.wisata.app.connection.callbacks.DefaultCallback
import com.wisata.app.data.Constant
import com.wisata.app.data.SharedPref
import com.wisata.app.model.WisataComplete
import com.wisata.app.utils.NetworkCheck
import kotlinx.android.synthetic.main.activity_my_wisata.*
import kotlinx.android.synthetic.main.include_load_failed.*
import kotlinx.android.synthetic.main.include_no_connection.*
import kotlinx.android.synthetic.main.include_no_data.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivityMyWisata : AppCompatActivity() {
    private var failedPage = 0
    private var totalItems = 0
    private var adapter: AdapterMyWisata? = null
    private var callbackCall: Call<CallbackWisata>? = null
    private var sharedPref: SharedPref? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_wisata)
        sharedPref = SharedPref(this)
        setupToolbar()
        initComponents()
        requestData(1)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Wisata Saya"
    }

    private fun initComponents() {
        recyclerView?.layoutManager = LinearLayoutManager(this)
        recyclerView?.setHasFixedSize(true)
        adapter = AdapterMyWisata(this, recyclerView, ArrayList())
        recyclerView?.adapter = adapter
        adapter?.setActivity(this)
        adapter?.setOnLoadMoreListener(object : AdapterMyWisata.OnLoadMoreListener {
            override fun onLoadMore(i: Int) {
                if (this@ActivityMyWisata.totalItems <= this@ActivityMyWisata.adapter!!.itemCount || i == 0) {
                    this@ActivityMyWisata.adapter!!.setLoaded()
                    return
                }
                this@ActivityMyWisata.requestData(i + 1)
            }
        })
        adapter?.setOnItemClickListener(object : AdapterMyWisata.OnItemClickListener {
            override fun onItemClick(view: View, wisata: WisataComplete, i: Int) {
                ActivityDetailWisata.navigate(this@ActivityMyWisata, wisata)
            }
        })
        adapter?.setOnPopupItemClickListener(object : AdapterMyWisata.OnPopupItemClickListener {
            override fun onEditItemClick(view: View, wisata: WisataComplete, i: Int) {
                ActivityFormWisata.navigate(this@ActivityMyWisata, true, wisata)
            }

            override fun onDeleteItemClick(view: View, wisata: WisataComplete, i: Int) {
                deleteWisata(wisataId = wisata.id?.toString()!!, position = i)
            }
        })
        swipeLayout.setOnRefreshListener {
            if (this@ActivityMyWisata.callbackCall != null && this@ActivityMyWisata.callbackCall!!.isExecuted) {
                this@ActivityMyWisata.callbackCall!!.cancel()
            }
            this@ActivityMyWisata.adapter!!.resetListData()
            this@ActivityMyWisata.requestData(1)
        }

        fab.setOnClickListener { ActivityFormWisata.navigate(this) }
    }

    private fun deleteWisata(wisataId: String, position: Int) {
        val callCallbackRemove = RestAdapter.createAPI().removeWisata(wisataId)
        callCallbackRemove.enqueue(object : Callback<DefaultCallback> {
            override fun onFailure(call: Call<DefaultCallback>?, t: Throwable?) {
                Toast.makeText(this@ActivityMyWisata, "Gagal menghapus wisata", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<DefaultCallback>?, response: Response<DefaultCallback>?) {
                response?.body()?.let {
                    if (it.status!!) {
                        Toast.makeText(this@ActivityMyWisata, "Berhasil menghapus wisata", Toast.LENGTH_SHORT).show()
                        adapter?.deleteAt(position)
                    } else {
                        Toast.makeText(this@ActivityMyWisata, it.msg, Toast.LENGTH_SHORT).show()
                    }
                    return@let
                }
            }
        })
    }

    private fun requestData(pageNumber: Int) {
        showFailedView(false)
        showNoItemView(false)
        showNoConnectionView(false)
        if (pageNumber == 1) {
            swipeProgress(true)
        } else {
            this.adapter!!.setLoading()
        }
        Handler().postDelayed({ this@ActivityMyWisata.requestList(pageNumber) }, 1000)
    }

    private fun displayData(items: ArrayList<WisataComplete>) {
        recyclerView.visibility = View.VISIBLE
        lytNoData.visibility = View.GONE
        lytLoadFailed.visibility = View.GONE
        lytNoConnection.visibility = View.GONE
        this.adapter!!.insertData(items)
        swipeProgress(false)
        if (items.size == 0) {
            showNoItemView(true)
        }
    }

    private fun requestList(pageNumber: Int) {
        if (sharedPref?.user != null) {
            this.callbackCall = RestAdapter.createAPI().getUserWisata(pageNumber = pageNumber, perPage = Constant.ITEMS_PER_REQUEST, userId = "${sharedPref?.user?.id}")
            this.callbackCall!!.enqueue(object : Callback<CallbackWisata> {
                override fun onResponse(call: Call<CallbackWisata>, response: Response<CallbackWisata>) {
                    val resp = response.body()
                    if (resp == null || !resp.status!!) {
                        this@ActivityMyWisata.onFailRequest(pageNumber)
                        return
                    }
                    this@ActivityMyWisata.totalItems = resp.totalItems!!
                    this@ActivityMyWisata.displayData(resp.data)
                }

                override fun onFailure(call: Call<CallbackWisata>, t: Throwable) {
                    if (!call.isCanceled) {
                        this@ActivityMyWisata.onFailRequest(pageNumber)
                    }
                }
            })
        }
    }

    private fun onFailRequest(pageNumber: Int) {
        this.failedPage = pageNumber
        this.adapter!!.setLoaded()
        swipeProgress(false)
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true)
        } else {
            showNoConnectionView(true)
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        swipeProgress(false)
        if (this.callbackCall != null && this.callbackCall!!.isExecuted) {
            this.callbackCall!!.cancel()
        }
    }

    private fun showFailedView(show: Boolean) {
        if (show) {
            recyclerView.visibility = View.GONE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.VISIBLE
            lytNoConnection.visibility = View.GONE
        } else {
            recyclerView.visibility = View.VISIBLE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        }
        btnRetry.setOnClickListener { this@ActivityMyWisata.requestData(this@ActivityMyWisata.failedPage) }
    }

    private fun showNoConnectionView(show: Boolean) {
        if (show) {
            recyclerView.visibility = View.GONE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        }
        btnConnectRetry.setOnClickListener { this@ActivityMyWisata.requestData(this@ActivityMyWisata.failedPage) }
    }

    private fun showNoItemView(show: Boolean) {
        if (show) {
            recyclerView.visibility = View.GONE
            lytNoData.visibility = View.VISIBLE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        } else {
            recyclerView.visibility = View.VISIBLE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        }
        btnReloadData.setOnClickListener { this@ActivityMyWisata.requestData(1) }
    }

    private fun swipeProgress(show: Boolean) {
        if (show) {
            swipeLayout.post { this@ActivityMyWisata.swipeLayout.isRefreshing = show }
        } else {
            swipeLayout.isRefreshing = show
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun navigate(activity: FragmentActivity) {
            val i = Intent(activity, ActivityMyWisata::class.java)
            activity.startActivity(i)
        }
    }
}
