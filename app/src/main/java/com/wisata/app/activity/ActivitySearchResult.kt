package com.wisata.app.activity

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.text.Html
import android.view.MenuItem
import android.view.View
import com.wisata.app.R
import com.wisata.app.adapter.AdapterWisata
import com.wisata.app.connection.RestAdapter
import com.wisata.app.connection.callbacks.CallbackWisata
import com.wisata.app.model.WisataComplete
import com.wisata.app.utils.NetworkCheck
import kotlinx.android.synthetic.main.activity_search_result.*
import kotlinx.android.synthetic.main.include_load_failed.*
import kotlinx.android.synthetic.main.include_no_connection.*
import kotlinx.android.synthetic.main.include_no_data.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivitySearchResult : AppCompatActivity() {

    private var options: String = ""
    private var latitude: String = ""
    private var longitude: String = ""
    private var qry: String = ""
    private var adapter: AdapterWisata? = null
    private var callbackCall: Call<CallbackWisata>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_result)
        qry = intent.extras[EXTRA_QRY] as String
        options = intent.extras[EXTRA_OPTIONS] as String
        val location = intent.extras[EXTRA_LOCATION] as Location
        location.let {
            latitude = it.latitude.toString()
            longitude = it.longitude.toString()
            return@let
        }
        setupToolbar()
        initComponents()
        requestData()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Pencarian"
    }

    private fun initComponents() {
        txtLabelResult?.text = Html.fromHtml("Hasil pencarian untuk <b>'$qry'</b>")
        recyclerView?.layoutManager = GridLayoutManager(this, 2)
        recyclerView?.setHasFixedSize(true)
        adapter = AdapterWisata(this, ArrayList())
        adapter?.isResultDisplayed = true
        recyclerView?.adapter = adapter
        adapter?.setOnItemClickListener(object : AdapterWisata.OnItemClickListener {
            override fun onItemClick(view: View, wisata: WisataComplete) {
                ActivityDetailWisata.navigate(this@ActivitySearchResult, wisata)
            }

        })
        swipeLayout.setOnRefreshListener {
            if (this@ActivitySearchResult.callbackCall != null && this@ActivitySearchResult.callbackCall!!.isExecuted) {
                this@ActivitySearchResult.callbackCall!!.cancel()
            }
            this@ActivitySearchResult.requestData()
        }
    }

    private fun requestData() {
        showFailedView(false)
        showNoItemView(false)
        showNoConnectionView(false)
        swipeProgress(true)
        Handler().postDelayed({ this@ActivitySearchResult.requestList() }, 1000)
    }

    private fun displayData(items: ArrayList<WisataComplete>) {
        recyclerView.visibility = View.VISIBLE
        lytNoData.visibility = View.GONE
        lytLoadFailed.visibility = View.GONE
        lytNoConnection.visibility = View.GONE
        this.adapter!!.setItems(items)
        swipeProgress(false)
        if (items.size == 0) {
            showNoItemView(true)
        }
    }

    private fun requestList() {
        this.callbackCall = RestAdapter.createAPI().getQueryWisata(options = options, qry = qry, latitude = this.latitude, longitude = this.longitude)
        this.callbackCall!!.enqueue(object : Callback<CallbackWisata> {
            override fun onResponse(call: Call<CallbackWisata>, response: Response<CallbackWisata>) {
                val resp = response.body()
                if (resp == null || !resp.status!!) {
                    this@ActivitySearchResult.onFailRequest()
                    return
                }
                this@ActivitySearchResult.displayData(resp.data)
            }

            override fun onFailure(call: Call<CallbackWisata>, t: Throwable) {
                if (!call.isCanceled) {
                    this@ActivitySearchResult.onFailRequest()
                }
            }
        })
    }

    private fun onFailRequest() {
        swipeProgress(false)
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true)
        } else {
            showNoConnectionView(true)
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        swipeProgress(false)
        if (this.callbackCall != null && this.callbackCall!!.isExecuted) {
            this.callbackCall!!.cancel()
        }
    }

    private fun showFailedView(show: Boolean) {
        if (show) {
            recyclerView.visibility = View.GONE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.VISIBLE
            lytNoConnection.visibility = View.GONE
        } else {
            recyclerView.visibility = View.VISIBLE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        }
        btnRetry.setOnClickListener { this@ActivitySearchResult.requestData() }
    }

    private fun showNoConnectionView(show: Boolean) {
        if (show) {
            recyclerView.visibility = View.GONE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        }
        btnConnectRetry.setOnClickListener { this@ActivitySearchResult.requestData() }
    }

    private fun showNoItemView(show: Boolean) {
        if (show) {
            recyclerView.visibility = View.GONE
            lytNoData.visibility = View.VISIBLE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        } else {
            recyclerView.visibility = View.VISIBLE
            lytNoData.visibility = View.GONE
            lytLoadFailed.visibility = View.GONE
            lytNoConnection.visibility = View.GONE
        }
        btnReloadData.setOnClickListener { this@ActivitySearchResult.requestData() }
    }

    private fun swipeProgress(show: Boolean) {
        if (show) {
            swipeLayout.post { this@ActivitySearchResult.swipeLayout.isRefreshing = show }
        } else {
            swipeLayout.isRefreshing = show
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val EXTRA_OPTIONS = "EXTRA_OPTIONS"
        const val EXTRA_LOCATION = "EXTRA_LOCATION"
        const val EXTRA_QRY = "EXTRA_QRY"

        fun navigate(activity: FragmentActivity, qry: String, options: String, location: Location?) {
            val i = Intent(activity, ActivitySearchResult::class.java)
            i.putExtra(EXTRA_QRY, qry)
            i.putExtra(EXTRA_OPTIONS, options)
            i.putExtra(EXTRA_LOCATION, location)
            activity.startActivity(i)
        }
    }
}
