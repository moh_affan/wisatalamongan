package com.wisata.app.utils

import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Bitmap.Config
import android.graphics.BitmapFactory
import android.graphics.BitmapFactory.Options
import android.graphics.Color
import android.graphics.PorterDuff.Mode
import android.net.Uri
import android.os.Build
import android.os.Build.VERSION
import android.provider.Settings
import android.support.annotation.ColorRes
import android.support.v4.app.FragmentActivity
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Patterns
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewPropertyAnimator
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.squareup.picasso.Picasso
import com.wisata.app.BuildConfig
import com.wisata.app.R
import com.wisata.app.model.Rating
import java.io.File
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

object Tools {

    val isLolipopOrHigher: Boolean
        get() = VERSION.SDK_INT >= 21

    val deviceName: String
        get() {
            val manufacturer = Build.MANUFACTURER
            val model = Build.MODEL
            return if (model.startsWith(manufacturer)) model else "$manufacturer $model"
        }

    val androidVersion: String
        get() = VERSION.RELEASE + BuildConfig.FLAVOR

    //Tools
    val windowWidth: Int
        get() = Resources.getSystem().displayMetrics.widthPixels

    fun needRequestPermission(): Boolean {
        return VERSION.SDK_INT > 22
    }

    fun setSystemBarColor(act: Activity, color: Int) {
        if (isLolipopOrHigher) {
            val window = act.window
            window.addFlags(Integer.MIN_VALUE)
            window.clearFlags(67108864)
            if (VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.statusBarColor = color
            }
        }
    }

    fun setSystemBarColor(act: Activity, color: String) {
        setSystemBarColor(act, Color.parseColor(color))
    }

    fun setSystemBarColorDarker(act: Activity, color: String) {
        setSystemBarColor(act, colorDarker(Color.parseColor(color)))
    }

    fun setSystemBarColorDarker(act: Activity, color: Int) {
        setSystemBarColor(act, colorDarker(color))
    }

    fun systemBarLolipop(act: Activity) {
        if (VERSION.SDK_INT >= 21) {
            val window = act.window
            window.addFlags(Integer.MIN_VALUE)
            window.clearFlags(67108864)
            window.statusBarColor = act.resources.getColor(R.color.colorPrimaryDark)
        }
    }

    fun rateAction(activity: Activity) {
        try {
            activity.startActivity(Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + activity.packageName)))
        } catch (e: ActivityNotFoundException) {
            activity.startActivity(Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + activity.packageName)))
        }

    }

//    fun showDialogAbout(activity: Activity) {
//        DialogUtils(activity).buildDialogInfo(R.string.title_about, R.string.content_about, R.string.OK, R.drawable.img_about, object : CallbackDialog {
//            override fun onPositiveClick(dialog: Dialog) {
//                dialog.dismiss()
//            }
//
//            override fun onNegativeClick(dialog: Dialog) {}
//        }).show()
//    }

    fun getVersionCode(ctx: Context): Int {
        try {
            return ctx.packageManager.getPackageInfo(ctx.packageName, 0).versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            return -1
        }

    }

//    fun getVersionNamePlain(ctx: Context): String {
//        try {
//            return ctx.packageManager.getPackageInfo(ctx.packageName, 0).versionName
//        } catch (e: PackageManager.NameNotFoundException) {
//            return ctx.getString(R.string.version_unknown)
//        }
//
//    }

    fun getFormattedDate(dateTime: Long): String {
        return SimpleDateFormat("MMMM dd, yyyy hh:mm").format(Date(dateTime))
    }

    fun getFormattedDateSimple(dateTime: Long): String {
        return SimpleDateFormat("MMM dd, yyyy").format(Date(dateTime))
    }

    fun displayImageOriginal(ctx: Context, img: ImageView, url: String) {
//        var vUrl = if (url.equals(Constant.getURLimgBook(""), ignoreCase = true)) Constant.getURLdefaultImgBook() else url
        try {
            Glide.with(ctx).load(url).apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).fitCenter()).into(img)
//            Picasso.with(ctx).load(url).centerCrop().into(img)
        } catch (e: Exception) {
        }

    }

    fun displayImageThumbnail(ctx: Context, img: ImageView, url: String, thumb: Float) {
//        var vUrl = if (url.equals(Constant.getURLimgBook(""), ignoreCase = true)) Constant.getURLdefaultImgBook() else url
        try {
            Glide.with(ctx).load(url).apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).fitCenter()).thumbnail(thumb).into(img)
//            Glide.with(ctx).load(url).crossFade().diskCacheStrategy(DiskCacheStrategy.NONE).thumbnail(thumb).into(img);
//            Picasso.with(ctx).load(url).fit().centerCrop().into(img)
        } catch (e: Exception) {
        }

    }

    fun colorDarker(color: Int): Int {
        val hsv = FloatArray(3)
        Color.colorToHSV(color, hsv)
        hsv[2] = hsv[2] * 0.9f
        return Color.HSVToColor(hsv)
    }

    fun colorBrighter(color: Int): Int {
        val hsv = FloatArray(3)
        Color.colorToHSV(color, hsv)
        hsv[2] = hsv[2] / 0.8f
        return Color.HSVToColor(hsv)
    }

    fun getGridSpanCount(activity: Activity): Int {
        val display = activity.windowManager.defaultDisplay
        val displayMetrics = DisplayMetrics()
        display.getMetrics(displayMetrics)
        return Math.round(displayMetrics.widthPixels.toFloat() / 180)
    }

    fun getFeaturedNewsImageHeight(activity: Activity): Int {
        val display = activity.windowManager.defaultDisplay
        val displayMetrics = DisplayMetrics()
        display.getMetrics(displayMetrics)
        return Math.round((displayMetrics.widthPixels - 10).toFloat() * 1.0f / 2.0f)
    }

    fun tintMenuIcon(context: Context, item: MenuItem, @ColorRes color: Int) {
        val wrapDrawable = DrawableCompat.wrap(item.icon)
        DrawableCompat.setTint(wrapDrawable, context.resources.getColor(color))
        item.icon = wrapDrawable
    }

    fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun getBitmap(file: File): Bitmap {
        val options = Options()
        options.inPreferredConfig = Config.ARGB_8888
        return BitmapFactory.decodeFile(file.absolutePath, options)
    }

//    fun getVersionName(ctx: Context): String {
//        try {
//            return ctx.getString(R.string.app_version) + " " + ctx.packageManager.getPackageInfo(ctx.packageName, 0).versionName
//        } catch (e: PackageManager.NameNotFoundException) {
//            return ctx.getString(R.string.version_unknown)
//        }
//    }

    fun copyToClipboard(context: Context, data: String) {
        (context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager).primaryClip = ClipData.newPlainText("clipboard", data)
        Toast.makeText(context, R.string.msg_copied_clipboard, Toast.LENGTH_SHORT).show()
    }


    fun getDeviceID(context: Context): String? {
        var deviceID: String? = Build.SERIAL
        if (deviceID == null || deviceID.trim { it <= ' ' }.isEmpty() || deviceID == "unknown") {
            try {
                deviceID = Settings.Secure.getString(context.contentResolver, "android_id")
            } catch (e: Exception) {
            }

        }
        return deviceID
    }

    fun convertToDip(context: Context, i: Int): Int {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i.toFloat(), context.resources.displayMetrics))
    }

    fun setSystemBarColor(activity: Activity) {
        if (VERSION.SDK_INT >= 21) {
            val window = activity.window
            window.addFlags(Integer.MIN_VALUE)
            window.clearFlags(67108864)
            window.statusBarColor = activity.resources.getColor(R.color.colorPrimaryDark)
        }
    }

    fun displayImageResource(context: Context, imageView: ImageView, resId: Int) {
        try {
//            Glide.with(context).load(Integer.valueOf(resId)).apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).fitCenter()).into(imageView)
            Picasso.with(context).load(resId).into(imageView)
        } catch (e: Exception) {
        }

    }

    //    public static void copyToClipboard(Context context, String str) {
    //        ((ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE)).setPrimaryClip(ClipData.newPlainText("clipboard", str));
    //        Toast.makeText(context, "Text copied to clipboard", Toast.LENGTH_SHORT).show();
    //    }

    fun scrollToBootom(nestedScrollView: NestedScrollView, view: View) {
        nestedScrollView.post { nestedScrollView.scrollTo(500, view.bottom) }
    }

    fun tintNavigationIcon(toolbar: Toolbar, resColorId: Int) {
        val navigationIcon = toolbar.navigationIcon
        navigationIcon!!.mutate()
        navigationIcon.setColorFilter(resColorId, Mode.SRC_ATOP)
    }

    fun tintMenuIcon(menu: Menu, resColorId: Int) {
        for (i2 in 0 until menu.size()) {
            val icon = menu.getItem(i2).icon
            if (icon != null) {
                icon.mutate()
                icon.setColorFilter(resColorId, Mode.SRC_ATOP)
            }
        }
    }

    fun rotate(view: View): Boolean {
        if (view.rotation == 0.0f) {
            view.animate().setDuration(200).rotation(180.0f)
            return true
        }
        view.animate().setDuration(200).rotation(0.0f)
        return false
    }

    @JvmOverloads
    fun animateRotation(z: Boolean, view: View, z2: Boolean = true): Boolean {
        var j: Long = 200
        val animate: ViewPropertyAnimator
        if (z) {
            animate = view.animate()
            if (!z2) {
                j = 0
            }
            animate.setDuration(j).rotation(180.0f)
            return true
        }
        animate = view.animate()
        if (!z2) {
            j = 0
        }
        animate.setDuration(j).rotation(0.0f)
        return false
    }

    fun getFullFormattedDate(l: Long): String {
        return SimpleDateFormat("EEE, MMM dd yyyy", Locale.getDefault()).format(Date(l))
    }

    fun toTitleCase(str: String): String {
        val toLowerCase = str.toLowerCase()
        val stringBuilder = StringBuilder()
        var obj: Any? = 1
        for (c in toLowerCase.toCharArray()) {
            var ch: Char = c
            if (Character.isSpaceChar(c)) {
                obj = 1
            } else if (obj != null) {
                ch = Character.toTitleCase(c)
                obj = null
            }
            stringBuilder.append(ch)
        }
        return stringBuilder.toString()
    }

    fun bVisibility(activity: Activity) {
        if (VERSION.SDK_INT >= 23) {
            val findViewById = activity.findViewById<View>(R.id.toolbar)
            findViewById.systemUiVisibility = findViewById.systemUiVisibility or 8192
        }
    }


    fun b(toolbar: Toolbar, i: Int) {
        try {
            val overflowIcon = toolbar.overflowIcon
            overflowIcon!!.mutate()
            overflowIcon.setColorFilter(i, Mode.SRC_ATOP)
        } catch (e: Exception) {
        }

    }

    fun getFormattedTime(l: Long): String {
        return SimpleDateFormat("hh:mm").format(Date(l))
    }

    fun getFormattedTime(activity: Activity) {
        if (VERSION.SDK_INT >= 21) {
            val window = activity.window
            window.addFlags(Integer.MIN_VALUE)
            window.statusBarColor = 0
        }
    }

    fun hideKeyboard(ctx: Context, view: View) {
        val imm = ctx.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

    }


    fun getGridSpanCount(activity: FragmentActivity): Int {
        val display = activity.windowManager.defaultDisplay
        val displayMetrics = DisplayMetrics()
        display.getMetrics(displayMetrics)
        return Math.round(displayMetrics.widthPixels.toFloat() / 200)
    }

    fun avgRating(ratings: ArrayList<Rating>): Float {
        var avg = 0.0f
        ratings.forEach {
            avg += it.rating!!
        }
        if (ratings.isNotEmpty()) {
            val a = avg / ratings.size
            val decimalFormat = DecimalFormat("#.#")
            val b = decimalFormat.format(a).replace(',','.')
            avg = b.toFloat()
        }
        return avg
    }
}
